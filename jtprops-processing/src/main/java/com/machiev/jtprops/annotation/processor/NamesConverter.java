/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.annotation.processor;

import com.machiev.jtprops.model.AccessorToNameConversionMode;

import static com.machiev.jtprops.model.AccessorToNameConversionMode.NONE;
import static com.machiev.jtprops.model.AccessorToNameConversionMode.UNSET;

/**
 * Utility class containing methods for name conversion.
 */
public class NamesConverter {

    private NamesConverter() {/*utility class*/}

    public static String methodToFieldName(String methodName) {

        if (methodName == null || methodName.isEmpty()) {
            throw new IllegalArgumentException("Missing method name");
        }

        //getName() get_name() GetName() isValid() is_Valid() Is_Valid() getter()

        String convertedName = methodName.replaceFirst("^(get|is|Get|Is)", "");
        if (convertedName.isEmpty()) {
            convertedName = methodName;
        }

        boolean prefixFound = (methodName.length() != convertedName.length());

        boolean underscoreFound = false;
        if (prefixFound) {
            while (convertedName.startsWith("_")) {
                convertedName = convertedName.substring(1);
                underscoreFound = true;
            }

            if (convertedName.isEmpty()) {
                convertedName = methodName;
            }
        }

        char firstLetter = convertedName.substring(0, 1).charAt(0);
        if (underscoreFound || Character.isUpperCase(firstLetter)) {
            firstLetter = Character.toLowerCase(firstLetter);
            convertedName = firstLetter + convertedName.substring(1);
        } else { //no underscore and does not start with upper case - take the whole method name
            convertedName = methodName;
        }

        return convertedName;
    }

    public static String methodToKeyName(String methodName, AccessorToNameConversionMode convertingMode, boolean noDuplicates) {

        String convertedName = methodToFieldName(methodName);

        switch(convertingMode) {
            case CAMELCASE_TO_DOTS:
            case CAMELCASE_TO_UNDERSCORE:
            case CAMELCASE_TO_HYPHEN:
                convertedName = camelCaseToSeparator(convertedName, convertingMode.getSeparator());
                break;
            case UNDERSCORE_TO_DOTS:
            case UNDERSCORE_TO_HYPHEN:
                convertedName = underscoreToSeparator(convertedName, convertingMode.getSeparator());
                break;
        }

        if (convertingMode != UNSET && convertingMode != NONE && noDuplicates) {
            convertedName = removeDuplicates(convertedName, convertingMode.getSeparator());
        }

        return convertedName;
    }

    private static String camelCaseToSeparator(String nameToConvert, String separator) {

        StringBuilder converted = new StringBuilder();

        for (Character ch : nameToConvert.toCharArray()) {
            if (Character.isUpperCase(ch)) {
                converted.append(separator).append(Character.toLowerCase(ch));
            } else {
                converted.append(ch);
            }
        }

        return converted.toString();
    }

    private static String underscoreToSeparator(String nameToConvert, String separator) {

        StringBuilder converted = new StringBuilder();

        for (char ch : nameToConvert.toCharArray()) {
            if ('_' == ch) {
                converted.append(separator);
            } else {
                converted.append(ch);
            }
        }

        return converted.toString();
    }

    private static String removeDuplicates(String toClean, String toRemove) {

        if (toRemove == null || toRemove.isEmpty()) {
            return toClean;
        }

        StringBuilder converted = new StringBuilder();
        int start = 0;
        int end;

        int index = toClean.indexOf(toRemove, start);
        int prevIndex = -1;
        while ( start < toClean.length() && index >= 0) {
            end = index + toRemove.length();
            if (prevIndex != index) {
                converted.append(toClean.substring(start, end));
            }
            start = end;
            prevIndex = end;
            index = toClean.indexOf(toRemove, start);
        }

        if (start < toClean.length()) {
            converted.append(toClean.substring(start));
        }

        return converted.toString();
    }

}
