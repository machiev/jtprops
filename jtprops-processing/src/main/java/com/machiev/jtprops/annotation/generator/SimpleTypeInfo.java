package com.machiev.jtprops.annotation.generator;

/**
 * Created by machiev on 22.10.14.
 */
public class SimpleTypeInfo {

    private String typeName = "";
    private String typePackage = "";
    private String defaultValue = "";

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypePackage() {
        return typePackage;
    }

    public void setTypePackage(String typePackage) {
        this.typePackage = typePackage;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isPackage() {
        return !typePackage.isEmpty();
    }
}
