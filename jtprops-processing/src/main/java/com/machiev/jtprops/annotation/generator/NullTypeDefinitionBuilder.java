package com.machiev.jtprops.annotation.generator;

/**
 * Created by machiev on 20.11.14.
 */

/**
 * Null implementation - used when type is not to be generated.
 */
public class NullTypeDefinitionBuilder implements TypeDefinitionBuilderInterface {

    @Override
    public String getSimpleClassName() {
        return null;
    }

    @Override
    public String getQualifiedClassName() {
        return null;
    }

    @Override
    public void appendImport(String importStatement) {

    }

    @Override
    public void addClassDefinitionStart(String classDefinition) {

    }

    @Override
    public void addClassDefinitionEnd(String classDefinition) {

    }

    @Override
    public void prependFieldDeclaration(String fieldStatement) {

    }

    @Override
    public void appendFieldDeclaration(String fieldStatement) {

    }

    @Override
    public void addConstructorDefinitionStart(String constructorStart) {

    }

    @Override
    public void prependToConstructor(String constructorStatement) {

    }

    @Override
    public void appendToConstructor(String constructorStatement) {

    }

    @Override
    public void addConstructorDefinitionEnd(String constructorEnd) {

    }

    @Override
    public void appendMethod(MethodDefinitionBuilder methodDefinitionBuilder) {

    }

    @Override
    public void addClassPackage(String classPackage) {

    }

    @Override
    public boolean hasMethods() {
        return false;
    }

    @Override
    public String build() {
        return null;
    }
}
