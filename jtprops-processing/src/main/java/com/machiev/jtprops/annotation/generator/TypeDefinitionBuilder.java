package com.machiev.jtprops.annotation.generator;

import java.util.*;

import static com.machiev.jtprops.annotation.generator.FormatElements.*;

/**
 * Created by machiev on 21.07.14.
 */
/**
 * Class template, containing data for a generator.
 */
public class TypeDefinitionBuilder implements TypeDefinitionBuilderInterface {

    private final String qualifiedClassName;

    private final String simpleClassName;

    private Set<String> imports = new TreeSet<String>();

    private String classDefinitionStart;

    private String classDefinitionEnd = "}";

    private List<String> fields = new ArrayList<String>();

    private MethodDefinitionBuilder constructorDefinition = new MethodDefinitionBuilder();

    private List<MethodDefinitionBuilder> methods = new ArrayList<MethodDefinitionBuilder>();

    private String classPackage;

    public TypeDefinitionBuilder(String packageName, String typeName) {
        this.simpleClassName = typeName;
        this.qualifiedClassName = (packageName.isEmpty()? "": packageName + '.') + typeName;
    }

    @Override
    public String getSimpleClassName() {
        return simpleClassName;
    }

    @Override
    public String getQualifiedClassName() {
        return qualifiedClassName;
    }

    @Override
    public void appendImport(String importStatement) {
        imports.add(importStatement);
    }

    @Override
    public void addClassDefinitionStart(String classDefinition) {
        classDefinitionStart = classDefinition;
    }

    @Override
    public void addClassDefinitionEnd(String classDefinition) {
        classDefinitionEnd = classDefinition;
    }

    @Override
    public void prependFieldDeclaration(String fieldStatement) {
        fields.add(0, fieldStatement);
    }

    @Override
    public void appendFieldDeclaration(String fieldStatement) {
        fields.add(fieldStatement);
    }

    @Override
    public void addConstructorDefinitionStart(String constructorStart) {
        this.constructorDefinition.addMethodStart(constructorStart);
    }

    @Override
    public void prependToConstructor(String constructorStatement) {
        constructorDefinition.prependMethodStatement(constructorStatement);
    }

    @Override
    public void appendToConstructor(String constructorStatement) {
        constructorDefinition.appendMethodStatement(constructorStatement);
    }

    @Override
    public void addConstructorDefinitionEnd(String constructorEnd) {
        constructorDefinition.addMethodEnd(constructorEnd);
    }

    @Override
    public void appendMethod(MethodDefinitionBuilder methodDefinitionBuilder) {
        methods.add(methodDefinitionBuilder);
    }

    @Override
    public void addClassPackage(String classPackage) {
        this.classPackage = classPackage;
    }

    @Override
    public boolean hasMethods() {
        return !methods.isEmpty();
    }

    @Override
    public String build() {
        StringBuilder classDef = new StringBuilder("//Generated code, do not edit").append(NL).append(NL);

        if (classPackage != null && !classPackage.isEmpty()) {
            classDef.append(classPackage).append(NL).append(NL);
        }

        for (String importDef : imports) {
            classDef.append(importDef).append(NL);
        }

        classDef.append(NL);
        classDef.append(classDefinitionStart).append(NL).append(NL);

        for (String field : fields) {
            classDef.append(VT).append(field).append(NL);
        }

        if (!constructorDefinition.isEmpty()) {
            constructorDefinition.indent(1);
            classDef.append(NL).append(constructorDefinition.build());
            classDef.append(NL);
        }

        for (MethodDefinitionBuilder method : methods) {
            method.indent(1);
            classDef.append(method.build()).append(NL);
        }
        classDef.append(classDefinitionEnd).append(NL);

        return classDef.toString();
    }
}
