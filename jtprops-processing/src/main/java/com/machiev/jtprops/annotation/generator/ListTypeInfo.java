package com.machiev.jtprops.annotation.generator;

import java.util.List;

/**
 * Created by machiev on 22.10.14.
 */
public class ListTypeInfo {

    private String typeName = "";
    private String typePackage = "";
    private String concreteTypeName = "";
    private String concreteTypePackage = "";
    private String elementTypeName = "";
    private String elementTypePackage = "";
    private String defaultValues = "";

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setTypePackage(String typePackage) {
        this.typePackage = typePackage;
    }

    public boolean isTypePackage() {
        return !typePackage.isEmpty();
    }

    public void setElementTypeName(String elementTypeName) {
        this.elementTypeName = elementTypeName;
    }

    public void setElementTypePackage(String elementTypePackage) {
        this.elementTypePackage = elementTypePackage;
    }

    public boolean isElementTypePackage() {
        return !elementTypePackage.isEmpty();
    }

    public void setConcreteTypeName(String concreteTypeName) {
        this.concreteTypeName = concreteTypeName;
    }

    public void setConcreteTypePackage(String concreteTypePackage) {
        this.concreteTypePackage = concreteTypePackage;
    }

    public boolean isConcreteTypePackage() {
        return !concreteTypePackage.isEmpty();
    }

    public String getConcreteTypePackage() {
        return concreteTypePackage;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getTypePackage() {
        return typePackage;
    }

    public String getElementTypeName() {
        return elementTypeName;
    }

    public String getElementTypePackage() {
        return elementTypePackage;
    }

    public String getConcreteTypeName() {
        return concreteTypeName;
    }

    public String getDefaultValues() {
        return defaultValues;
    }

    public void setDefaultValues(String defaultValues) {
        this.defaultValues = defaultValues;
    }
}
