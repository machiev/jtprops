package com.machiev.jtprops.annotation.generator;

/**
 * Created by machiev on 16.11.14.
 */
public final class FormatElements {

    /* Platform-independent new line */
    static final String NL = String.format("%n");

    /* Vertical tab - indentation */
    static String VT = "    ";

    public static void setVT(String verticalTab) {
        VT = verticalTab;
    }
}
