/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.annotation.processor;

import com.machiev.jtprops.model.AccessorToNameConversionMode;
import com.machiev.jtprops.reader.PropertyReader;

import java.util.Properties;

/**
 * Annotation processor options supplied via command line.
 */
public class ProcessorOptions {

    /**
     * Indentation used in generated code.
     */
    public enum IndentType {
        SPACE(' '),
        TAB('\t');

        private final char character;
        IndentType(char c) {
            character = c;
        }

        public char getCharacter() {
            return character;
        }
    }

    private static final String PREFIX = "jtp";

    private final Properties properties;

    private boolean generatePropertyFile;
    private AccessorToNameConversionMode keyConversionMode = AccessorToNameConversionMode.CAMELCASE_TO_DOTS;
    private boolean keyConversionSingleSeparator = true;
    private int indentSize = 4;
    private IndentType indentType = IndentType.SPACE;

    public ProcessorOptions(Properties properties, PropertyReader reader) {
        this.properties = properties;
        generatePropertyFile = reader.get(properties, PREFIX + ".generate.property.file", generatePropertyFile);

        keyConversionMode = reader.get(properties, PREFIX + ".key.conversion.mode", keyConversionMode);

        keyConversionSingleSeparator = reader.get(properties, PREFIX + ".key.conversion.single.separator", keyConversionSingleSeparator);

        indentSize  = reader.get(properties, PREFIX + ".indent.size", indentSize);

        indentType = reader.get(properties, PREFIX + ".indent.type", indentType);
    }

    public boolean isGeneratePropertyFile() {
        return generatePropertyFile;
    }

    public AccessorToNameConversionMode getKeyConversionMode() {
        return keyConversionMode;
    }

    public boolean hasKeyConversionMode() {
        return properties.containsKey(PREFIX + ".key.conversion.mode");
    }
    public boolean isKeyConversionSingleSeparator() {
        return keyConversionSingleSeparator;
    }

    public int getIndentSize() {
        return indentSize;
    }

    public IndentType getIndentType() {
        return indentType;
    }

}
