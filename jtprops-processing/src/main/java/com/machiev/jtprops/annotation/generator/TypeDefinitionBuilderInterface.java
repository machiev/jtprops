package com.machiev.jtprops.annotation.generator;

/**
 * Created by machiev on 20.11.14.
 */
public interface TypeDefinitionBuilderInterface {
    String getSimpleClassName();

    String getQualifiedClassName();

    void appendImport(String importStatement);

    void addClassDefinitionStart(String classDefinition);

    void addClassDefinitionEnd(String classDefinition);

    void prependFieldDeclaration(String fieldStatement);

    void appendFieldDeclaration(String fieldStatement);

    void addConstructorDefinitionStart(String constructorStart);

    void prependToConstructor(String constructorStatement);

    void appendToConstructor(String constructorStatement);

    void addConstructorDefinitionEnd(String constructorEnd);

    void appendMethod(MethodDefinitionBuilder methodDefinitionBuilder);

    void addClassPackage(String classPackage);

    boolean hasMethods();

    String build();
}
