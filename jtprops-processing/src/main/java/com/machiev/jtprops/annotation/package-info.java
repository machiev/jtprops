/**
 * Contains Java Typed Properties (JTProps) annotation processor implementing {@link javax.annotation.processing.Processor}.
 *
 * JTProps annotations processor implements code generator for annotations defined in {@link com.machiev.jtprops.model} package.
 */
package com.machiev.jtprops.annotation;
