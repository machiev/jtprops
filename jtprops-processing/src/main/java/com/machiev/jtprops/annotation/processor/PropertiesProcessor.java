/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.annotation.processor;

import com.machiev.jtprops.annotation.generator.*;
import com.machiev.jtprops.model.*;
import com.machiev.jtprops.reader.PropertyReader;
import com.machiev.jtprops.reader.SimplePropertyReader;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

import static com.machiev.jtprops.model.OptionalFlag.*;
import static java.lang.String.format;
import static javax.lang.model.element.ElementKind.*;

/**
 * Annotation processor class
 */
@SupportedAnnotationTypes(value = {
        "com.machiev.jtprops.model.PropertiesClass",
        "com.machiev.jtprops.model.Property",
        "com.machiev.jtprops.model.ListProperty"})
@SupportedOptions({"jtp.generate.property.file", "jtp.key.conversion.mode", "jtp.key.conversion.single.separator", "jtp.indent.size", "jtp.indent.type"})
public class PropertiesProcessor extends AbstractProcessor {

    private Elements elementsUtil;
    private Types typesUtil;
    private ProcessorOptions options;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);

        elementsUtil = processingEnv.getElementUtils();
        typesUtil = processingEnv.getTypeUtils();

        Properties properties = new Properties();
        properties.putAll(processingEnv.getOptions());
        printOptions(properties);
        options = new ProcessorOptions(properties, new SimplePropertyReader());

        char[] indentArray = new char[options.getIndentSize()];
        Arrays.fill(indentArray, options.getIndentType().getCharacter());
        FormatElements.setVT(String.valueOf(indentArray));
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        for (Element classElement : roundEnv.getElementsAnnotatedWith(PropertiesClass.class)) {

            PropertiesClass propertiesClass = classElement.getAnnotation(PropertiesClass.class);
            TypeDefinitionBuilderInterface classBuilder = new TypeDefinitionBuilder(propertiesClass.packageName(), propertiesClass.className());
            TypeDefinitionBuilderInterface extendedInterface = new NullTypeDefinitionBuilder();
            if (!propertiesClass.extendedInterfaceName().isEmpty() && classElement.getKind() == INTERFACE) {
                extendedInterface = new TypeDefinitionBuilder(propertiesClass.packageName(), propertiesClass.extendedInterfaceName());
            }

            for (Element methodElement : roundEnv.getElementsAnnotatedWith(Property.class)) {
                if (methodElement.getEnclosingElement().equals(classElement)) {
                    Property property = methodElement.getAnnotation(Property.class);
                    handleAnnotatedMethod(classBuilder, extendedInterface, methodElement, propertiesClass, property);
                }
            }

            for (Element methodElement : roundEnv.getElementsAnnotatedWith(ListProperty.class)) {
                if (methodElement.getEnclosingElement().equals(classElement)) {
                    ListProperty property = methodElement.getAnnotation(ListProperty.class);
                    handleAnnotatedMethod(classBuilder, extendedInterface, methodElement, propertiesClass, property);
                }
            }

            if (extendedInterface.hasMethods()) {
                generateExtendedInterfaceDefinition(extendedInterface, propertiesClass, (TypeElement) classElement);
                generateSource(extendedInterface);
            }

            generateClassDefinition(classBuilder, extendedInterface, propertiesClass, (TypeElement) classElement);
            generateSource(classBuilder);
        }

        return true;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    private void generateSource(TypeDefinitionBuilderInterface typeDefinitionBuilder) {
        if (typeDefinitionBuilder.hasMethods()) {
            Writer writer = createSourceFile(typeDefinitionBuilder.getQualifiedClassName());
            try {
                writer.append(typeDefinitionBuilder.build());
            } catch (IOException e) {
                throw new Error(e);
            } finally {
                closeFile(writer);
            }
        }
    }

    private Writer createSourceFile(String qualifiedClassName) {
        printNote(format("Creating source file for: '%s'", qualifiedClassName));

        try {
            JavaFileObject jfo = processingEnv.getFiler().createSourceFile(qualifiedClassName);
            return new BufferedWriter(jfo.openWriter());

        } catch (IOException e) {
            String msg = "Cannot create source file for: '" + qualifiedClassName + "' " + e.getMessage();
            printError(msg);
            throw new Error(msg, e);
        }
    }

    private void generateExtendedInterfaceDefinition(TypeDefinitionBuilderInterface extendedInterface, PropertiesClass propertiesClass, TypeElement classElement) {
        printNote("Generating interface: " + extendedInterface.getQualifiedClassName());

        extendedInterface.addClassPackage(format("package %s;", propertiesClass.packageName()));

        String parentPackage = getPackageOf(classElement);
        if (!parentPackage.isEmpty() && !parentPackage.equals(propertiesClass.packageName())) {
            extendedInterface.appendImport(format("import %s.*;", parentPackage));
        }

        String parentTypeName = classElement.getQualifiedName().toString();
        if (!parentPackage.isEmpty()) {
            parentTypeName = parentTypeName.substring(parentPackage.length()+1);
        }
        extendedInterface.addClassDefinitionStart(format(
                "public interface %s extends %s {",
                propertiesClass.extendedInterfaceName(),
                parentTypeName)
        );
    }

    private void generateClassDefinition(TypeDefinitionBuilderInterface classBuilder, TypeDefinitionBuilderInterface extendedInterface, PropertiesClass propertiesClass, TypeElement classElement) {
        printNote("Generating class: " + propertiesClass.className());

        classBuilder.addClassPackage(format("package %s;", propertiesClass.packageName()));

        String parentPackage = getPackageOf(classElement);
        if (!parentPackage.isEmpty() && !parentPackage.equals(propertiesClass.packageName())) {
            classBuilder.appendImport(format("import %s.*;", parentPackage));
        }

        classBuilder.appendImport(format("import %s;", Properties.class.getCanonicalName()));
        classBuilder.appendImport(format("import %s;", PropertyReader.class.getCanonicalName()));
        classBuilder.prependFieldDeclaration(format("private final Properties properties;%n"));
        classBuilder.prependToConstructor(format("this.properties = properties;%n"));

        String extendKind;
        boolean isInterface = false;
        switch(classElement.getKind()) {
            case INTERFACE:
                extendKind = "implements";
                isInterface = true;
                break;
            case CLASS:
                extendKind = "extends";
                break;
            default:
                printError(format("Unexpected element type: %s for: %s", classElement.getKind(), classElement.getSimpleName()));
                throw new IllegalArgumentException();
        }
        String abstractQualifier = propertiesClass.abstractClass() == OptionalFlag.TRUE? " abstract ": " ";
        String parentTypeName;
        if (isInterface && extendedInterface.hasMethods()) {//if there is extended interface specified and there are extended methods
            parentTypeName = extendedInterface.getSimpleClassName();
        } else {
            parentTypeName = classElement.getQualifiedName().toString();
            if (!parentPackage.isEmpty()) {
                parentTypeName = parentTypeName.substring(parentPackage.length()+1);
            }
        }

        classBuilder.addClassDefinitionStart(format(
                "public%sclass %s %s %s {",
                abstractQualifier,
                propertiesClass.className(),
                extendKind,
                parentTypeName)
        );
        classBuilder.addConstructorDefinitionStart(format("public %s(Properties properties, PropertyReader reader) {", propertiesClass.className()));
        classBuilder.addConstructorDefinitionEnd("}");
        classBuilder.addClassDefinitionEnd("}");
    }

    private void handleAnnotatedMethod(
            TypeDefinitionBuilderInterface classBuilder,
            TypeDefinitionBuilderInterface extendedInterface,
            Element methodElement,
            PropertiesClass propertiesClass,
            Property property) {

        ExecutableElement method = (ExecutableElement) methodElement;
        TypeMirror returnType = method.getReturnType();

        SimpleTypeInfo simpleType = extractSimpleTypeInfo(returnType, property.defaultValue());

        String methodName = method.getSimpleName().toString();
        String fieldName = NamesConverter.methodToFieldName(methodName);
        String propertyName = createPropertyName(propertiesClass, property.name(), methodName);

        if (simpleType.isPackage()) {
            classBuilder.appendImport(format("import %s;", simpleType.getTypePackage()));
        }
        classBuilder.appendFieldDeclaration(format("private %s %s = %s;", simpleType.getTypeName(), fieldName, simpleType.getDefaultValue()));
        classBuilder.appendToConstructor(format("%s = reader.get(properties, \"%s\", %1$s);", fieldName, propertyName));

        MethodDefinitionBuilder methodDefinitionBuilder = new MethodDefinitionBuilder();
        methodDefinitionBuilder.addMethodStart(format("public %s %s() {", simpleType.getTypeName(), methodName));
        methodDefinitionBuilder.appendMethodStatement(format("return %s;", fieldName));
        classBuilder.appendMethod(methodDefinitionBuilder);

        if (generateChecker(propertiesClass.generateCheckers(), property.generateChecker())) {
            generateCheckerMethod(classBuilder, extendedInterface, fieldName, propertyName);
        }
    }

    private void handleAnnotatedMethod(TypeDefinitionBuilderInterface classBuilder, TypeDefinitionBuilderInterface extendedInterface, Element methodElement, PropertiesClass propertiesClass, ListProperty property) {

        ExecutableElement method = (ExecutableElement) methodElement;
        TypeMirror returnTypeMirror = method.getReturnType();

        ListTypeInfo listTypeInfo = extractListTypeInfo(returnTypeMirror, property.defaultValue());

        String returnTypeStr = format("%s<%s>", listTypeInfo.getTypeName(), listTypeInfo.getElementTypeName());
        String methodName = method.getSimpleName().toString();
        String fieldName = NamesConverter.methodToFieldName(methodName);
        String propertyName = createPropertyName(propertiesClass, property.name(), methodName);

        if (listTypeInfo.isTypePackage()) {
            classBuilder.appendImport(format("import %s;", listTypeInfo.getTypePackage()));
        }
        if (listTypeInfo.isConcreteTypePackage()) {
            classBuilder.appendImport(format("import %s;", listTypeInfo.getConcreteTypePackage()));
        }
        if (listTypeInfo.isElementTypePackage()) {
            classBuilder.appendImport(format("import %s;", listTypeInfo.getElementTypePackage()));
        }

        classBuilder.appendFieldDeclaration(
                format("private %s %s = new %s<%s>();",
                        returnTypeStr,
                        fieldName,
                        listTypeInfo.getConcreteTypeName(),
                        listTypeInfo.getElementTypeName()));

        MethodDefinitionBuilder methodDefinitionBuilder = new MethodDefinitionBuilder();
        methodDefinitionBuilder.addMethodStart(format("public %s %s() {", returnTypeStr, methodName));
        methodDefinitionBuilder.appendMethodStatement(format("return %s;", fieldName));
        classBuilder.appendMethod(methodDefinitionBuilder);

        String defaultValuesString;
        if (listTypeInfo.getDefaultValues().isEmpty()) {
            defaultValuesString = format("new %s[0]", listTypeInfo.getElementTypeName());
        } else {
            defaultValuesString = listTypeInfo.getDefaultValues();
        }
        classBuilder.appendToConstructor(
                format("%s = reader.getList(properties, \"%s\", \"%s\", %s);",
                        fieldName, propertyName, property.separator(), defaultValuesString));

        if (generateChecker(propertiesClass.generateCheckers(), property.generateChecker())) {
            generateCheckerMethod(classBuilder, extendedInterface, fieldName, propertyName);
        }
    }

    private boolean generateChecker(OptionalFlag generateCheckersForClass, OptionalFlag generateCheckerForMethod) {

        if (generateCheckerForMethod != UNSET) {
            return generateCheckerForMethod == TRUE;
        } else if (generateCheckersForClass != UNSET) {
            return generateCheckersForClass == TRUE;
        }
        return false;
    }

    private String createPropertyName(PropertiesClass propertiesClass, String providedPropertyName, String methodName) {
        String propertyName = providedPropertyName;
        if (propertyName.isEmpty()) {
            AccessorToNameConversionMode conversionMode = propertiesClass.conversionMode();
            if (conversionMode == AccessorToNameConversionMode.UNSET) {
                conversionMode = options.getKeyConversionMode();
            }
            propertyName = NamesConverter.methodToKeyName(methodName, conversionMode, options.isKeyConversionSingleSeparator());
        }
        return propertyName;
    }

    private void generateCheckerMethod(TypeDefinitionBuilderInterface classBuilder, TypeDefinitionBuilderInterface extendedInterface, String fieldName, String propertyName) {
        String checkerMethodName = format("has%s%s", Character.toUpperCase(fieldName.charAt(0)), fieldName.substring(1));
        MethodDefinitionBuilder methodDefinitionBuilder = new MethodDefinitionBuilder();
        methodDefinitionBuilder.addMethodStart(format("public boolean %s() {", checkerMethodName));
        methodDefinitionBuilder.appendMethodStatement(format("return properties.containsKey(\"%s\");", propertyName));
        classBuilder.appendMethod(methodDefinitionBuilder);

        MethodDefinitionBuilder methodDeclarationBuilder = new MethodDefinitionBuilder();
        methodDeclarationBuilder.addMethodStart(format("boolean %s();", checkerMethodName));
        methodDeclarationBuilder.addMethodEnd("");
        extendedInterface.appendMethod(methodDeclarationBuilder);
    }

    private String getPackageOf(Element element) {
        return elementsUtil.getPackageOf(element).getQualifiedName().toString();
    }

    private SimpleTypeInfo extractSimpleTypeInfo(TypeMirror type, String providedDefault) {
        SimpleTypeInfo typeInfo = new SimpleTypeInfo();

        boolean isDefaultProvided = !isNullOrEmpty(providedDefault);

        String defaultValue;
        TypeKind typeKind = type.getKind();
        if (typeKind.isPrimitive()) {
            typeInfo.setDefaultValue(isDefaultProvided? providedDefault: defaultValueForPrimitiveType(typeKind));
            typeInfo.setTypeName(type.toString());
        } else if (typeKind == TypeKind.DECLARED) {
            TypeElement declaredTypeElement = (TypeElement)typesUtil.asElement(type);
            typeInfo.setTypePackage(declaredTypeElement.getQualifiedName().toString());

            switch (declaredTypeElement.getKind()) {
            case ENUM:
                for (Element element : declaredTypeElement.getEnclosedElements()) {
                    if (element.getKind() == ENUM_CONSTANT) {
                        String enumName = declaredTypeElement.getSimpleName().toString();
                        typeInfo.setTypeName(enumName);
                        if (isDefaultProvided) {
                            defaultValue = enumName + "." + providedDefault;
                        } else {
                            defaultValue = enumName + "." + element.getSimpleName().toString();
                        }
                        typeInfo.setDefaultValue(defaultValue);
                        break;
                    }
                }
                break;
            case CLASS:
                typeInfo.setTypeName(declaredTypeElement.getSimpleName().toString());

                if (String.class.getCanonicalName().equals(declaredTypeElement.getQualifiedName().toString())) {
                    typeInfo.setDefaultValue(format("\"%s\"", providedDefault));
                } else {//expecting simple type of Integer, Boolean, etc.
                    TypeMirror unboxedType = typesUtil.unboxedType(type);
                    if (isDefaultProvided) {
                        typeInfo.setDefaultValue(providedDefault);
                    } else {
                        typeInfo.setDefaultValue(defaultValueForPrimitiveType(unboxedType.getKind()));
                    }
                }
                break;
            default:
                String errorMsg = format("Invalid element kind supplied: %s", declaredTypeElement.getKind());
                printError(errorMsg);
                throw new IllegalArgumentException(errorMsg);
            }
        } else {
            String errorMsg = format("Invalid type kind supplied: %s", typeKind);
            printError(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }
        return typeInfo;
    }

    private String defaultValueForPrimitiveType(TypeKind primitiveTypeKind) {

        String defaultValue;
        switch (primitiveTypeKind) {
            case CHAR:
            case INT:
            case SHORT:
            case BYTE:
                defaultValue = "0";
                break;
            case LONG:
                defaultValue = "0L";
                break;
            case FLOAT:
            case DOUBLE:
                defaultValue = "0.0";
                break;
            case BOOLEAN:
                defaultValue = "false";
                break;
            default:
                String errorMsg = format("Invalid type kind supplied: %s, expected simple type or primitive", primitiveTypeKind);
                printError(errorMsg);
                throw new IllegalArgumentException(errorMsg);
        }

        return defaultValue;
    }

    private ListTypeInfo extractListTypeInfo(TypeMirror type, String[] providedDefault) {
        if (type.getKind() != TypeKind.DECLARED) {
            String errorMsg = "Invalid type for list property: " + type.getKind();
            printError(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }

        DeclaredType declaredType = (DeclaredType)type;
        TypeElement declaredTypeElement = (TypeElement)typesUtil.asElement(declaredType);
        ListTypeInfo listTypeInfo = new ListTypeInfo();

        if (declaredTypeElement.getKind() == INTERFACE) {
            if (List.class.getCanonicalName().equals(declaredTypeElement.getQualifiedName().toString())) {
                listTypeInfo.setTypeName(List.class.getSimpleName());
                listTypeInfo.setTypePackage(List.class.getCanonicalName());
                listTypeInfo.setConcreteTypeName(ArrayList.class.getSimpleName());//TODO: get some default
                listTypeInfo.setConcreteTypePackage(ArrayList.class.getCanonicalName());

                TypeElement element = getArgumentTypeElementFromGeneric(declaredType);
                listTypeInfo.setElementTypeName(element.getSimpleName().toString());
                listTypeInfo.setElementTypePackage(element.getQualifiedName().toString());
                String quote = "";
                if (String.class.getCanonicalName().equals(element.getQualifiedName().toString())) {
                    quote = "\"";
                }
                StringBuilder defaultValuesAsString = new StringBuilder();
                for (String defaultElement : providedDefault) {
                    defaultValuesAsString.append(quote).append(defaultElement).append(quote).append(", ");
                }
                int length = defaultValuesAsString.length();
                if (length > 0) {
                    defaultValuesAsString.delete(length - 2, length);
                }
                listTypeInfo.setDefaultValues(defaultValuesAsString.toString());
            }
        } else if (declaredTypeElement.getKind() == CLASS) {
            //TODO: handle some concrete list classes
            String errorMsg = "Currently only List interface is supported, supplied: " + declaredTypeElement.getSimpleName();
            printError(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        } else {
            String errorMsg = "Unsupported declared type: " + declaredType.getKind() + " for: " + declaredTypeElement.getSimpleName();
            printError(errorMsg);
            throw new IllegalArgumentException(errorMsg);
        }

        return listTypeInfo;
    }

    private TypeElement getArgumentTypeElementFromGeneric(DeclaredType declaredType) {
        List<? extends TypeMirror> typeArgumentMirrors = declaredType.getTypeArguments();

        if (typeArgumentMirrors.size() == 1) {
            TypeMirror typeArgMirror = typeArgumentMirrors.get(0);
            if(typeArgMirror.getKind() == TypeKind.DECLARED) {
                return (TypeElement)typesUtil.asElement(typeArgMirror);
            }
        }

        throw new IllegalArgumentException(format("Invalid type for supplied generic type: %s", declaredType.asElement().getSimpleName()));
    }

    private void closeFile(Writer writer) {

        try {
            writer.close();

        } catch (IOException e) {
            throw new Error(e);
        }
    }

    private void printOptions(Properties properties) {
        StringBuilder optionsString = new StringBuilder("Processor options:" + (properties.isEmpty()? " NONE": ""));
        for (String key : properties.stringPropertyNames()) {
            optionsString.append(format("%n%s = %s", key, properties.getProperty(key)));
        }
        printNote(optionsString.toString());
    }

    private void printNote(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
    }

    private void printWarning(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, msg);
    }

    private void printError(String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, msg);
    }

    private static boolean isNullOrEmpty(String s) {
        return (s == null || s.isEmpty());
    }

}
