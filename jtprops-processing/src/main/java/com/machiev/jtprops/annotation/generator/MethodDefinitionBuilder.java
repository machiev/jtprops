package com.machiev.jtprops.annotation.generator;

import java.util.ArrayList;
import java.util.List;

import static com.machiev.jtprops.annotation.generator.FormatElements.*;

/**
 * Created by machiev on 16.11.14.
 */
public class MethodDefinitionBuilder {

    private String methodStart = "";
    private String methodEnd = "}";
    private List<String> methodStatements = new ArrayList<String>();
    private int indentLevel;

    public void addMethodStart(String methodStart) {
        this.methodStart = methodStart;
    }

    public void addMethodEnd(String methodEnd) {
        this.methodEnd = methodEnd;
    }

    public void prependMethodStatement(String methodStatement) {
        this.methodStatements.add(0, methodStatement);
    }

    public void appendMethodStatement(String methodStatement) {
        this.methodStatements.add(methodStatement);
    }

    public boolean isEmpty() {
        return methodStart.isEmpty();
    }

    /**
     * Indents whole method with specified level - number of vertical tab characters.
     * @param level specifies level of indentation, values start from 0. 0 means no indentation
     */
    public void indent(int level) {
        this.indentLevel = level;
    }

    public String build() {
        StringBuilder indent = new StringBuilder();
        for (int i = 0; i < indentLevel; i++) {
            indent.append(VT);
        }

        StringBuilder methodDefinition = new StringBuilder()
                .append(indent)
                .append(methodStart)
                .append(NL);

        for (String statement : methodStatements) {
            methodDefinition.append(indent).append(VT).append(statement).append(NL);
        }

        if (!methodEnd.isEmpty()) {
            methodDefinition.append(indent).append(methodEnd).append(NL);
        }

        return methodDefinition.toString();
    }
}
