/**
 * Implementation of annotation processor's interface {@link javax.annotation.processing.Processor}.
 *
 * Process annotations defined in {@link com.machiev.jtprops.model} package.
 */
package com.machiev.jtprops.annotation.processor;
