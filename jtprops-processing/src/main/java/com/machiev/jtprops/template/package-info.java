/**
 * Defines Java Typed Properties generator based on file template.
 * Contains parser, code generator and command line interface for user's interaction.
 */
package com.machiev.jtprops.template;
