package com.machiev.jtprops.template.generator;

import com.machiev.jtprops.template.type.*;

import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * Created by machiev on 03.05.14.
 */
public class FieldGenerator {

    public String generate(String name, IntegerEntryType integerEntryType) {

        return "int " + name + " = " + integerEntryType.asString() + ';';
    }

    public String generate(String name, StringEntryType stringEntryType) {
        return "String " + name + " = " + stringEntryType.asString() + ';';
    }

    public String generate(String name, BooleanEntryType booleanEntryType) {
        return "boolean " + name + " = " + booleanEntryType.asString() + ';';
    }

    public String generate(String name, EnumEntryType enumEntryType) {
        StringBuilder enumBuilder = new StringBuilder(enumEntryType.getEnumName());
        enumBuilder.append(" ").append(name).append(" = ");
        enumBuilder.append(enumEntryType.getEnumName()).append('.').append(enumEntryType.asString());
        enumBuilder.append(';');

        return enumBuilder.toString();
    }

    public String generate(String name, ListEntryType listEntryType) {

        return "List<" + listEntryType.getElementType() + "> " + name + " = new ArrayList<" + listEntryType.getElementType() + ">();";
    }

    public String generateEnumDefinition(EnumEntryType enumEntryType) {
        StringBuilder enumBuilder = new StringBuilder("enum ");
        enumBuilder.append(enumEntryType.getEnumName()).append(" { ");

        Iterator<String> iterator = enumEntryType.getEnumValueList().iterator();
        while (iterator.hasNext()) {
            enumBuilder.append(iterator.next());
            if (iterator.hasNext()) {
                enumBuilder.append(", ");
            }
        }
        enumBuilder.append(" }");

        return enumBuilder.toString();
    }

    /**
     * Dispatcher method used to invoke proper polymorphic generate method.
     * @param fieldName name of the field to generate definition of
     * @param entryType entry type
     * @return string containing definition of the class' field
     */
    public String generate(String fieldName, EntryType entryType) {

        try
        {
            Method thisPolymorphic = this.getClass().getMethod("generate",
                    new Class[] { String.class, entryType.getClass() });

            return (String) thisPolymorphic.invoke(this, fieldName, entryType);
        }
        catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Unknown entry type: " + entryType.getClass() + ", cannot generate field", e);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Generation error", e);
        }
    }

}
