package com.machiev.jtprops.template.type;

import java.util.*;

/**
 * Created by machiev on 29.03.14.
 */
public class ListEntryType<E extends EntryType> implements EntryType {

    private final List<E> list;
    private final String elementType;
    private final String delimiter;
    private final String separator;

    public ListEntryType(List<E> list, String elementType, String delimiter, String separator) {

        this.list = (list==null)? Collections.<E>emptyList(): list;
        this.elementType = elementType;
        this.delimiter = delimiter;
        this.separator = separator;
    }

    @Override
    public String asString() {

        StringBuilder builder = new StringBuilder();
        Iterator<E> iterator = list.iterator();

        while (iterator.hasNext()) {
            builder.append(iterator.next().asString());
            if (iterator.hasNext()) {
                builder.append(',');
            }
        }

        return builder.toString();
    }

//    public String getValueList

    public List<E> getList() {
        return list;
    }

    public String getElementType() {
        return elementType;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public static void main(String[] args) {

//        List<? super Integer> intList = new ArrayList<Integer>();
//        List<? super Double> numList = new ArrayList<Number>();
//
//        intList = new ArrayList<Integer>();
//
//        intList.add(Integer.valueOf(4));
//        Number i = intList.get(0);
//        Integer ii = intList.get(0);
    }

}
