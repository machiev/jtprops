package com.machiev.jtprops.template.parser;

import com.machiev.jtprops.template.entry.PropertyEntry;
import com.machiev.jtprops.template.exception.EntryException;
import com.machiev.jtprops.template.exception.RepeatedEntryException;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by machiev on 29.03.14.
 */
public class Parser {

    private static final Pattern TYPE_DESCRIPTION_PATTERN = Pattern.compile("^\\s*#+\\s*type\\s*:\\s*");
    private static final Pattern ENTRY_DEFINITION_PATTERN = Pattern.compile("^\\s*(:?\\w+\\.?)+\\s*=");
    private static final Pattern COMMENT_PATTERN = Pattern.compile("^\\s*#+");

    private final Iterator<String> lineIterator;
    private final boolean skipNotDescribedEntry;//skip not described entry or treat it as string type
    private final int listMaxElementsCount; //TODO: add some default

    private boolean descriptionFlag;
    private String lastDescriptionLine;
    private String typeDefinition;
    private int currentLineNo;

    private Set<PropertyEntry> entries = new LinkedHashSet<PropertyEntry>();

    public Parser(Iterator<String> lineIterator, boolean skipNotDescribedEntry, int listMaxElementsCount) {

        this.lineIterator = lineIterator;
        this.skipNotDescribedEntry = skipNotDescribedEntry;
        this.listMaxElementsCount = listMaxElementsCount;
    }

    public Collection<PropertyEntry> parse() throws EntryException {

        while(lineIterator.hasNext()) {
            currentLineNo++;
            parseLine(lineIterator.next());
        }

        return entries;
    }

    private void parseLine(String line) throws EntryException {

        if (line == null || line.trim().isEmpty()) {
            return;
        }

        if (TYPE_DESCRIPTION_PATTERN.matcher(line).find()) {
            parseTypeDescription(line);
        } else if (ENTRY_DEFINITION_PATTERN.matcher(line).find()) {
            parseEntryDefinition(line);
        } else if (COMMENT_PATTERN.matcher(line).find()) {
            //skip comment
        } else {
            throw new EntryException("Line: " + currentLineNo + ", Invalid sequence: [" + line + "]");
        }
    }

    private void parseTypeDescription(String line) throws EntryException {
        if (descriptionFlag) {
            throw new EntryException("Line: " + currentLineNo + ", Description line without property definition: [" + lastDescriptionLine + ']');
        }

        descriptionFlag = true;
        lastDescriptionLine = line;

        String[] typeDefElems = line.split("type\\s*:");
        if (typeDefElems.length != 2) {
            throw new EntryException("Line: " + currentLineNo + ", Invalid description line format: [" + line + "]");
        }

        typeDefinition = typeDefElems[1].trim();
    }

    private void parseEntryDefinition(String line) throws EntryException  {

        if (!descriptionFlag && skipNotDescribedEntry) {
            return;
        }

        if (!descriptionFlag) { //assume type String
            typeDefinition = "string";
        }

        descriptionFlag = false;

        String[] keyValue = line.split("=", 2); //when there is no value after '=' we get an empty string
        if (keyValue.length != 2) {
            throw new EntryException("Line: " + currentLineNo + ", Invalid definition line format: [" + line + "]");
        }

        String entryName = keyValue[0].trim();
        String entryDefaultValue = keyValue[1].trim();

        PropertyEntry propertyEntry;
        try {
            propertyEntry = new PropertyEntry(
                    entryName,
                    new TypeFactory(typeDefinition, entryDefaultValue, listMaxElementsCount).create());
        } catch(EntryException e) {
            throw new EntryException("Line: " + currentLineNo + ", " + e.getMessage(), e);
        }
        if (!entries.add(propertyEntry)) {
            throw new RepeatedEntryException("Line: " + currentLineNo + ", Entry already defined: " + entryName);
        }
    }

}
