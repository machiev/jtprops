package com.machiev.jtprops.template.exception;

/**
 * Created by machiev on 29.03.14.
 */
public class EntryException extends Exception {

    public EntryException() {
        super();
    }

    public EntryException(String message) {
        super(message);
    }

    public EntryException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntryException(Throwable cause) {
        super(cause);
    }
}
