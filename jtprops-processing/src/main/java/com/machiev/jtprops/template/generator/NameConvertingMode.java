package com.machiev.jtprops.template.generator;

/**
 * Created by machiev on 02.05.14.
 *
 *
 * Mode for converting property name to a variable name.
 */
public enum NameConvertingMode {
    DOTS_TO_CAMEL_CASE,
    DOTS_TO_UNDERSCORE
}
