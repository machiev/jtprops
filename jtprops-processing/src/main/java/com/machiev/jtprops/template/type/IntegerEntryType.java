package com.machiev.jtprops.template.type;

/**
 * Created by machiev on 29.03.14.
 */
public class IntegerEntryType implements EntryType {

    private int value;

    public IntegerEntryType(int value) {
        this.value = value;
    }

    @Override
    public String asString() {
        return Integer.toString(value);
    }

}
