package com.machiev.jtprops.template.generator;

import com.machiev.jtprops.template.type.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by machiev on 04.05.14.
 */
public class GetterGenerator {

    private String indent;

    public GetterGenerator(String indent) {

        this.indent = indent;
    }

    public List<String> generate(String fieldName, String methodName, EntryType entryType) {

        try
        {
            Method thisPolymorphic = this.getClass().getMethod("generate",
                    new Class[] { String.class, String.class, entryType.getClass() });

            return (List<String>) thisPolymorphic.invoke(this, fieldName, methodName, entryType);
        }
        catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Unknown entry type: " + entryType.getClass() + ", cannot generate getter", e);
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Generation error", e);
        }
    }

    public List<String> generate(String fieldName, String methodName, IntegerEntryType entryType) {

        List<String> lines = new ArrayList<String>();

        lines.add("int get" + methodName + "() {");
        lines.add(indent + "return " + fieldName + ';');
        lines.add("}");

        return lines;
    }

    public List<String> generate(String fieldName, String methodName, StringEntryType entryType) {

        List<String> lines = new ArrayList<String>();

        lines.add("String get" + methodName + "() {");
        lines.add(indent + "return " + fieldName + ';');
        lines.add("}");

        return lines;
    }

    public List<String> generate(String fieldName, String methodName, BooleanEntryType entryType) {

        List<String> lines = new ArrayList<String>();

        lines.add("boolean get" + methodName + "() {");
        lines.add(indent + "return " + fieldName + ';');
        lines.add("}");

        return lines;
    }

    public List<String> generate(String fieldName, String methodName, ListEntryType entryType) {

        List<String> lines = new ArrayList<String>();

        lines.add("List<" + entryType.getElementType() + "> get" + methodName + "() {");
        lines.add(indent + "return " + fieldName + ';');
        lines.add("}");

        return lines;
    }

    public List<String> generate(String fieldName, String methodName, EnumEntryType entryType) {

        List<String> lines = new ArrayList<String>();

        lines.add(entryType.getEnumName() + " get" + methodName + "() {");
        lines.add(indent + "return " + fieldName + ';');
        lines.add("}");

        return lines;
    }
}
