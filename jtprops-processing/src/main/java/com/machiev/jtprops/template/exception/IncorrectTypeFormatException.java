package com.machiev.jtprops.template.exception;

/**
 * Created by machiev on 27.04.14.
 */
public class IncorrectTypeFormatException extends EntryException {

    public IncorrectTypeFormatException(String message) {
        super(message);
    }
}
