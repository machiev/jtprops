package com.machiev.jtprops.template.type;

import java.util.List;

/**
 * Created by machiev on 27.04.14.
 */
public class EnumEntryType implements EntryType {

    private final String enumName;
    private final List<String> enumValueList;
    private final String defaultValue;

    public EnumEntryType(String enumName, List<String> enumValueList, String defaultValue) {

        this.enumName = enumName;
        this.enumValueList = enumValueList;
        this.defaultValue = defaultValue;
    }

    @Override
    public String asString() {
        return defaultValue;
    }

    public String getEnumName() {
        return enumName;
    }

    public List<String> getEnumValueList() {
        return enumValueList;
    }
}
