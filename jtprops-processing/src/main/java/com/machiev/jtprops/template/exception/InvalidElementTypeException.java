package com.machiev.jtprops.template.exception;

/**
 * Created by machiev on 29.03.14.
 */
public class InvalidElementTypeException extends EntryException {

    public InvalidElementTypeException() {
        super();
    }

    public InvalidElementTypeException(String message) {
        super(message);
    }

    public InvalidElementTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidElementTypeException(Throwable cause) {
        super(cause);
    }
}
