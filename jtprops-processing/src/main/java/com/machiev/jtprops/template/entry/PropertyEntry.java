package com.machiev.jtprops.template.entry;

import com.machiev.jtprops.template.type.EntryType;

import java.util.Locale;

/**
 * Created by machiev on 28.03.14.
 */
public class PropertyEntry {

    private final String entryName;

    private final EntryType entryType;

    public PropertyEntry(String entryName, EntryType entryType) {
        this.entryName = entryName;
        this.entryType = entryType;
    }

    public String getEntryName() {
        return entryName;
    }

    public EntryType getEntryType() {
        return entryType;
    }

    /**
     * Two properties are equal when they have equal name without case sensitivity.
     */
    @Override
    public boolean equals(Object obj) {

        if (obj instanceof PropertyEntry) {
            PropertyEntry that = (PropertyEntry)obj;
            return (entryName == that.entryName) || entryName != null && entryName.equalsIgnoreCase(that.entryName);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 29;

        if (entryName != null) {
            hash += 29 * entryName.toLowerCase(Locale.ROOT).hashCode();
        }

        return hash;
    }

    @Override
    public String toString() {

        return "Name: [" + entryName + "], type: " + entryType.getClass().getSimpleName();
    }
}
