package com.machiev.jtprops.template.generator;

import com.machiev.jtprops.template.exception.EntryException;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by machiev on 05.04.14.
 */
public class PropertyNameConverter {

    private static String BEFORE_NUMBER_PREFIX = "$";  //TODO: make it configurable
    private static Pattern STARTS_WITH_NUMBER_PATTERN = Pattern.compile("^\\d");

    private final NameConvertingMode mode;

    public PropertyNameConverter(NameConvertingMode mode) {

        this.mode = mode;
    }

    public String toFieldName(String propertyName) throws EntryException {

        String[] elements= propertyName.split("\\.");

        int nextElementIndex = 0;
        String firstElement = elements[nextElementIndex++];
        if (firstElement.isEmpty()) {
            if (elements.length > nextElementIndex) {
                firstElement = elements[nextElementIndex++];
            } else {
                throw new EntryException("Incorrect property name: " + propertyName);
            }
        }

        Matcher matcher = STARTS_WITH_NUMBER_PATTERN.matcher(firstElement);
        StringBuilder convertedName = new StringBuilder();
        if (matcher.lookingAt()) {
            convertedName.append(BEFORE_NUMBER_PREFIX);
        }
        convertedName.append(firstElement);

        for (; nextElementIndex < elements.length; nextElementIndex++) {

            if (mode == NameConvertingMode.DOTS_TO_UNDERSCORE) {

                convertedName.append('_').append(elements[nextElementIndex]);
            } else {

                String firstChar = elements[nextElementIndex].substring(0, 1);
                convertedName
                        .append(firstChar.toUpperCase(Locale.ROOT))
                        .append(elements[nextElementIndex].substring(1));
            }
        }

        return convertedName.toString();
    }
}
