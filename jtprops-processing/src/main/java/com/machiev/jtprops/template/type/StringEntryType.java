package com.machiev.jtprops.template.type;

/**
 * Created by machiev on 29.03.14.
 */
public class StringEntryType implements EntryType {

    private String value = "";

    public StringEntryType(String value) {
        this.value = (value==null)? "": value;
    }

    @Override
    public String asString() {
        return "\"" + value + "\"";
    }
}
