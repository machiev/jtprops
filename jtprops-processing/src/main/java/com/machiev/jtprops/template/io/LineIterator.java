package com.machiev.jtprops.template.io;

import java.io.*;
import java.util.Iterator;

/**
 * Created by machiev on 15.06.14.
 */
public class LineIterator implements Iterator<String>, Closeable {

    /* Path to a file */
    private final File filePath;

    private Reader ioReader;

    private BufferedReader reader;

    private String currentLine;

    public LineIterator(File filePath) {

        this.filePath = filePath;
        this.ioReader = null;
    }

    public LineIterator(Reader ioReader) {

        this.filePath = null;
        this.ioReader = ioReader;
    }

    @Override
    public boolean hasNext() {

        try {
            if (reader == null) {
                if (filePath != null) {
                    reader = new BufferedReader(new FileReader(filePath));
                } else if (ioReader != null) {
                    reader = new BufferedReader(ioReader);
                }
            }
            currentLine = reader.readLine();

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Error accessing a file", e);
        } catch (IOException e) {
            throw new IllegalArgumentException("Error reading a file", e);
        }

        return currentLine != null;
    }

    @Override
    public String next() {

        String copy = currentLine;
        currentLine = null;

        return copy;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void close() throws IOException {

        if (reader != null) {
            reader.close();
            reader = null;
        }
    }
}
