package com.machiev.jtprops.template.type;

/**
 * Created by machiev on 28.03.14.
 */
public interface EntryType {

    String asString();

}
