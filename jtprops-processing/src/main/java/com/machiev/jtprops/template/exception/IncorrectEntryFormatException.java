package com.machiev.jtprops.template.exception;

/**
 * Created by machiev on 29.03.14.
 */
public class IncorrectEntryFormatException extends EntryException {

    public IncorrectEntryFormatException() {
        super();
    }

    public IncorrectEntryFormatException(String message) {
        super(message);
    }

    public IncorrectEntryFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectEntryFormatException(Throwable cause) {
        super(cause);
    }
}
