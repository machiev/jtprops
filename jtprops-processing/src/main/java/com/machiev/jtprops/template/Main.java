package com.machiev.jtprops.template;

import com.machiev.jtprops.template.AppOptionParseException;
import com.machiev.jtprops.template.entry.PropertyEntry;
import com.machiev.jtprops.template.exception.EntryException;
import com.machiev.jtprops.template.generator.NameConvertingMode;
import com.machiev.jtprops.template.generator.PropertiesWrapperGenerator;
import com.machiev.jtprops.template.io.LineIterator;
import com.machiev.jtprops.template.parser.Parser;

import java.io.*;
import java.util.Collection;
import java.util.Properties;


/**
 * Class implementing command line interface for properties template parser and code generator.
 */
public class Main {

    private static final String appName = "pwrgen";

    private static final String OPTION_PACKAGE_NAME         = "-p"; //TODO: consider changing this to enum
    private static final String OPTION_CLASS_NAME           = "-c";
    private static final String OPTION_DIR_NAME             = "-d";
    private static final String OPTION_UNDERSCORE_MODE_NAME = "-u";
    private static final String OPTION_ELEMENTS_COUNT_NAME  = "-e";
    private static final String OPTION_FILE_NAME            = "FILENAME";

    private static final int CONFIG_INDENT_SIZE = 4;
    private static final int CONFIG_MAX_ELEMENTS_IN_LIST = 100;

    /**
     * Usage
     *      propsgen [options] FILE
     *
     * Options:
     * -p PACKAGE_NAME
     *      mandatory, generated package name, PACKAGE_NAME should be valid Java package name
     * -c CLASS_NAME
     *      mandatory, generated class name
     * -d DIR
     *      optional, output directory where generated class will be stored, defaults to current
     * -u
     *      optional, property name to accessor name conversion mode underscore, default is camel case
     * -e NUM
     *      optional, maximum elements in the parsed list
     *
     * @param args
     */
    public static void main(String[] args) {

        try {
            Properties arguments = parseArguments(args);
            checkMandatoryOptions(arguments);

            String packageName = arguments.getProperty(OPTION_PACKAGE_NAME);
            String className = arguments.getProperty(OPTION_CLASS_NAME);
            String fileName = arguments.getProperty(OPTION_FILE_NAME);
            String outputDirName = ".";
            if (arguments.containsKey(OPTION_DIR_NAME)) {
                outputDirName = arguments.getProperty(OPTION_DIR_NAME);
            }
            NameConvertingMode convertingMode = NameConvertingMode.DOTS_TO_CAMEL_CASE;
            if (arguments.containsKey(OPTION_UNDERSCORE_MODE_NAME)) {
                convertingMode = NameConvertingMode.DOTS_TO_UNDERSCORE;
            }
            int maxListElements = CONFIG_MAX_ELEMENTS_IN_LIST;
            if (arguments.containsKey(OPTION_ELEMENTS_COUNT_NAME)) {
                maxListElements = Integer.valueOf(arguments.getProperty(OPTION_ELEMENTS_COUNT_NAME));
            }

            LineIterator lineIterator = new LineIterator(new File(fileName));
            Parser parser = new Parser(lineIterator, true, maxListElements);

            Collection<PropertyEntry> entries = parser.parse();
            System.out.println("Parsed entries:");
            for (PropertyEntry entry : entries) {
                System.out.println(entry);
            }
            lineIterator.close();

            PropertiesWrapperGenerator wrapperGenerator =
                    new PropertiesWrapperGenerator(entries, packageName, className, convertingMode, CONFIG_INDENT_SIZE);

            String generatedClass = wrapperGenerator.generate();
            System.out.println("Generated class:\n" + generatedClass);

            File outputFile = prepareOutputFile(outputDirName, packageName, className);
            FileWriter output = new FileWriter(outputFile);
            output.write(generatedClass);
            output.close();

        } catch (EntryException e) {
            System.out.println(e.getMessage());
        } catch (AppOptionParseException e) {
            System.out.println(e.getMessage());
            System.out.println(usage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static File prepareOutputFile(String outputDirName, String packageName, String className) {

        String packageDir = packageName.replace(".", File.separator);

        File outputDir = new File(outputDirName, packageDir);
        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }

        String outFileName = className + ".java";

        return new File(outputDir, outFileName);
    }

    private static String usage() {
        return "Usage:\n" +
                appName + " [options] FILENAME\n\n" +
                "FILENAME specified a property file template\n" +
                "Options:\n" +
                "    -p PACKAGE_NAME\n" +
                "         mandatory, generated package name, PACKAGE_NAME should be a valid Java package name\n" +
                "    -c CLASS_NAME\n" +
                "         mandatory, generated class name\n" +
                "    -d DIR\n" +
                "         optional, output directory where generated class will be stored, defaults to current\n" +
                "    -u\n" +
                "         optional, property name to accessor name conversion mode underscore, default is camel case\n" +
                "    -e NUM\n" +
                "         optional, maximum elements in the parsed list, defaults to " + CONFIG_MAX_ELEMENTS_IN_LIST + "\n";
    }

    private static void checkMandatoryOptions(Properties arguments) throws AppOptionParseException {

        if (!arguments.containsKey(OPTION_PACKAGE_NAME)) {
            throw new AppOptionParseException("Missing package name option");
        } else if (!arguments.containsKey(OPTION_CLASS_NAME)) {
            throw new AppOptionParseException("Missing class name option");
        } else if (!arguments.containsKey(OPTION_FILE_NAME)) {
            throw new AppOptionParseException("Missing file name");
        }
    }

    private static Properties parseArguments(String[] args) throws AppOptionParseException {

        Properties arguments = new Properties();

        for (int i = 0; i < args.length; i++) {
            if (OPTION_PACKAGE_NAME.equals(args[i])) {
                if (i + 1 < args.length) {
                    arguments.setProperty(args[i], args[++i]);
                } else {
                    throw new AppOptionParseException("Option " + OPTION_PACKAGE_NAME + " is missing a value");
                }
            } else if (OPTION_CLASS_NAME.equals(args[i])) {
                if (i + 1 < args.length) {
                    arguments.setProperty(args[i], args[++i]);
                } else {
                    throw new AppOptionParseException("Option " + OPTION_CLASS_NAME + " is missing a value");
                }
            } else if (OPTION_DIR_NAME.equals(args[i])) {
                if (i + 1 < args.length) {
                    arguments.setProperty(args[i], args[++i]);
                } else {
                    throw new AppOptionParseException("Option " + OPTION_DIR_NAME + " is missing a value");
                }
            } else if (OPTION_UNDERSCORE_MODE_NAME.equals(args[i])) {
                arguments.setProperty(args[i], "");
            } else if (OPTION_ELEMENTS_COUNT_NAME.equals(args[i])) {
                if (i + 1 < args.length) {
                    try {
                        Integer.valueOf(args[i + 1]);
                    } catch (NumberFormatException nfe) {
                        throw new AppOptionParseException("Option " + OPTION_ELEMENTS_COUNT_NAME + " requires integer value");
                    }
                    arguments.setProperty(args[i], args[++i]);
                } else {
                    throw new AppOptionParseException("Option " + OPTION_ELEMENTS_COUNT_NAME + " is missing a value");
                }
            } else {
                arguments.setProperty(OPTION_FILE_NAME, args[i]);
            }

        }

        return arguments;
    }
}
