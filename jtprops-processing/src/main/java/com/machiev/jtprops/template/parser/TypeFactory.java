package com.machiev.jtprops.template.parser;

import com.machiev.jtprops.template.exception.EntryException;
import com.machiev.jtprops.template.exception.IncorrectEntryFormatException;
import com.machiev.jtprops.template.exception.IncorrectTypeFormatException;
import com.machiev.jtprops.template.exception.InvalidElementTypeException;
import com.machiev.jtprops.template.type.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by machiev on 30.03.14.
 */
public class TypeFactory {

    private static final Pattern LIST_TYPE_AND_DELIMITER_PATTERN = Pattern.compile("[Ll][Ii][Ss][Tt]\\s*<\\s*([a-zA-Z]+)\\s*>\\s*\\{\\s*(.*)\\s*\\}");
    private static final Pattern ENUM_TYPE_PATTERN = Pattern.compile("[Ee][Nn][Uu][Mm]\\s*<\\s*([\\w_$]+)\\s*>\\s*\\{\\s*(.*)\\s*\\}");
    private static final Pattern ENUM_VALUE_PATTERN = Pattern.compile("[\\w_$]+");
    private static final int MAX_ELEMENTS_NUMBER_DEFAULT = 1024;

    private final String typeDefinition;
    private final String defaultValue;
    private int maxElementsNumber;

    public TypeFactory(String typeDefinition, String defaultValue) {
        this(typeDefinition, defaultValue, MAX_ELEMENTS_NUMBER_DEFAULT);
    }
    public TypeFactory(String typeDefinition, String defaultValue, int maxElementsNumber) {//TODO: add some properties to configure factory

        this.typeDefinition = typeDefinition;
        this.defaultValue = defaultValue;
        this.maxElementsNumber = maxElementsNumber;
    }

    public EntryType create() throws EntryException {

        if ("integer".equalsIgnoreCase(typeDefinition)) {
            return createInteger(defaultValue);

        } else if ("string".equalsIgnoreCase(typeDefinition)) {
            return new StringEntryType(defaultValue);

        } else if ("boolean".equalsIgnoreCase(typeDefinition)) {
            return createBoolean(defaultValue);

        } else if (typeDefinition.toLowerCase(Locale.ROOT).startsWith("enum")) {
            return createEnum(typeDefinition, defaultValue);
        } else if (typeDefinition.toLowerCase(Locale.ROOT).startsWith("list")) {
            return createList(typeDefinition, defaultValue);

        } else {
            throw new EntryException("Incorrect or unsupported type found in description: [" + typeDefinition + ']');
        }
    }

    private IntegerEntryType createInteger(String stringValue) throws EntryException {
        try {

            return new IntegerEntryType(Integer.valueOf(stringValue.trim().isEmpty()?"0": stringValue.trim()));

        } catch(NumberFormatException nfe) {
            throw new IncorrectEntryFormatException("incorrect integer format", nfe);
        }
    }

    private BooleanEntryType createBoolean(String stringValue) {
        return new BooleanEntryType(Boolean.valueOf(stringValue.trim()));
    }

    private ListEntryType createList(String typeDefinition, String stringValue) throws EntryException {

        Matcher matcher = LIST_TYPE_AND_DELIMITER_PATTERN.matcher(typeDefinition);

        if (matcher.lookingAt()) {
            if (matcher.groupCount() == 2) {

                String elementType = matcher.group(1).toLowerCase(Locale.ROOT);
                String delimiter = matcher.group(2);
                String separator;
                if (delimiter.isEmpty()) { //by default it will be whitespace
                    separator = " ";
                    delimiter = "\\s+";
                } else {
                    separator = delimiter;
                    delimiter = Pattern.quote(delimiter);
                }
                String[] elements = stringValue.split(delimiter, maxElementsNumber);

                if ("integer".equals(elementType)) {
                    return parseIntegerList(elements, delimiter, separator);
                } else if ("string".equals(elementType)) {
                    return parseStringList(elements, delimiter, separator);
                } else {
                    throw new InvalidElementTypeException("Unsupported element type: " + elementType);
                }

            } else  {
                throw new IncorrectTypeFormatException("Incorrect format in type definition: " + typeDefinition);
            }
        } else {
            throw new IncorrectTypeFormatException("Incorrect format in type definition: " + typeDefinition);
        }
    }

    private EntryType createEnum(String typeDefinition, String defaultValue) throws EntryException {

        Matcher matcher = ENUM_TYPE_PATTERN.matcher(typeDefinition);

        if (matcher.lookingAt()) {
            if (matcher.groupCount() == 2) {

                String enumName = matcher.group(1).toLowerCase(Locale.ROOT);
                String enumValues = matcher.group(2);

                List<String> enumValueList = new ArrayList<String>();
                for (String value : enumValues.split(",")) {
                    String trimmedValue = value.trim();
                    if (ENUM_VALUE_PATTERN.matcher(trimmedValue).matches()) {
                        enumValueList.add(trimmedValue);
                    } else {
                        throw new IncorrectTypeFormatException("Incorrect format in enum value(s) definition: [" + trimmedValue + "]");
                    }
                }

                if (enumValueList.isEmpty()) {
                    throw new IncorrectTypeFormatException("Enum values not specified: [" + typeDefinition + "]");
                }

                if (!enumValueList.contains(defaultValue)) {
                    throw new IncorrectEntryFormatException("Enum value undefined: [" + defaultValue + "]");
                }

                return new EnumEntryType(enumName, enumValueList, defaultValue);

            } else  {
                throw new IncorrectTypeFormatException("Incorrect format in enum value(s) definition: [" + typeDefinition + "]");
            }
        } else {
            throw new IncorrectTypeFormatException("Incorrect format in enum type definition: [" + typeDefinition + "]");
        }
    }

    private ListEntryType<IntegerEntryType> parseIntegerList(String[] elements, String delimiter, String separator) throws IncorrectEntryFormatException {
        List<IntegerEntryType> list = new ArrayList<IntegerEntryType>();

        for (String element : elements) {
            try {
                if (!element.trim().isEmpty()) {
                    int intValue = Integer.valueOf(element.trim());
                    list.add(new IntegerEntryType(intValue));
                }
            } catch (NumberFormatException nfe) {
                throw new IncorrectEntryFormatException("Invalid integer element type");
            }
        }

        return new ListEntryType<IntegerEntryType>(list, "Integer", delimiter, separator);
    }

    private ListEntryType<StringEntryType> parseStringList(String[] elements, String delimiter, String separator) {
        List<StringEntryType> list = new ArrayList<StringEntryType>();

        for (String element : elements) {
            list.add(new StringEntryType(element.trim()));
        }

        return new ListEntryType<StringEntryType>(list, "String", delimiter, separator);
    }
}
