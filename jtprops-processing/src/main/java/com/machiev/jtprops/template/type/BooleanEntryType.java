package com.machiev.jtprops.template.type;

/**
 * Created by machiev on 30.03.14.
 */
public class BooleanEntryType implements EntryType {

    private boolean value;

    public BooleanEntryType(boolean value) {
        this.value = value;
    }

    @Override
    public String asString() {
        return Boolean.toString(value);
    }

}
