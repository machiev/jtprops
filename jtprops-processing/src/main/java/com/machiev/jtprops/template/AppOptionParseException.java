package com.machiev.jtprops.template;

/**
 * Created by machiev on 15.06.14.
 */
public class AppOptionParseException extends Exception {

    public AppOptionParseException(String message) {
        super(message);
    }

    public AppOptionParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
