package com.machiev.jtprops.template.exception;

/**
 * Created by machiev on 30.03.14.
 */
public class RepeatedEntryException extends EntryException {
    public RepeatedEntryException() {
        super();
    }

    public RepeatedEntryException(String message) {
        super(message);
    }

    public RepeatedEntryException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepeatedEntryException(Throwable cause) {
        super(cause);
    }
}
