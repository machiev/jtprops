package com.machiev.jtprops.template.generator;

import com.machiev.jtprops.template.entry.PropertyEntry;
import com.machiev.jtprops.template.exception.EntryException;
import com.machiev.jtprops.template.type.EnumEntryType;
import com.machiev.jtprops.template.type.ListEntryType;

import java.util.*;

/**
 * Created by machiev on 02.05.14.
 */
public class PropertiesWrapperGenerator {

    private final Collection<PropertyEntry> entries;
    private final String destinationPackage;
    private final String className;
    private final String indent;
    private final PropertyNameConverter nameConverter;

    public PropertiesWrapperGenerator(Collection<PropertyEntry> entries,
                                      String destinationPackage,
                                      String className,
                                      NameConvertingMode convertingMode,
                                      int indentSize) {

        this.entries = entries;
        this.destinationPackage = destinationPackage;
        this.className = className;
        nameConverter = new PropertyNameConverter(convertingMode);

        StringBuilder indentBuilder = new StringBuilder(indentSize);
        while(indentSize-- > 0) {
            indentBuilder.append(' ');
        }
        indent = indentBuilder.toString();
    }

    public String generate() throws EntryException {
        StringBuilder output = new StringBuilder();

        output.append("//Generated code, do not edit\n\n");
        append(output, "package ", destinationPackage, ";\n\n");

        output.append("import java.util.*;\n\n");
        output.append("import com.machiev.jtprops.reader.PropertyReader;\n\n"); //needed for PropertyReader

        append(output, "public class ", className, " {\n\n");
        generateEnumDefinitions(output);
        output.append("\n\n");

        output.append(indent).append("private final Properties properties;\n");
        output.append(indent).append("private final PropertyReader reader;\n\n");
        generateFields(output);
        output.append('\n');

        append(output, indent, "public ", className, "(Properties properties, PropertyReader reader) {\n");
        append(output, indent, indent, "this.properties = properties;\n");
        append(output, indent, indent, "this.reader = reader;\n\n");
        generateListInitialization(output);
        output.append(indent).append("\n");
        generatePropertiesReading(output);
        output.append(indent).append("}\n\n");

        generateAccessors(output);

        output.append("}\n"); //end of class

        return output.toString();
    }

    private void generateEnumDefinitions(StringBuilder output) {
        FieldGenerator fieldGenerator = new FieldGenerator();

        for (PropertyEntry entry : entries) {
            if (entry.getEntryType() instanceof EnumEntryType ) {
                output.append(indent).append("public static ");
                output.append(fieldGenerator.generateEnumDefinition((EnumEntryType) entry.getEntryType()));
            }
        }
    }

    private void generateFields(StringBuilder output) throws EntryException {

        FieldGenerator fieldGenerator = new FieldGenerator();

        for (PropertyEntry entry : entries) {
            String fieldName = nameConverter.toFieldName(entry.getEntryName());

            String field = fieldGenerator.generate(fieldName, entry.getEntryType());
            output.append(indent).append("private ").append(field).append('\n');
        }
    }

    private void generateListInitialization(StringBuilder output) throws EntryException {
        for (PropertyEntry entry : entries) {
            if (entry.getEntryType() instanceof ListEntryType) {
                String defaultValue = entry.getEntryType().asString();
                System.out.println("DEFAULT VAL: [" + defaultValue + "]");
                if (defaultValue.isEmpty() || "\"\"".equals(defaultValue)) {
                    continue;
                }
                String fieldName = nameConverter.toFieldName(entry.getEntryName());
                append(output, indent, indent, "Collections.addAll(", fieldName, ", ");
                output.append(defaultValue).append(");\n");
            }
        }
    }

    private void generatePropertiesReading(StringBuilder output) throws EntryException {
        for (PropertyEntry entry : entries) {

            String fieldName = nameConverter.toFieldName(entry.getEntryName());
            output.append(indent).append(indent);

            if (entry.getEntryType() instanceof ListEntryType) {
                ListEntryType listEntryType = (ListEntryType) entry.getEntryType();
                append(output, fieldName, " = ", "reader.get", listEntryType.getElementType(), "List(");
                String escapedDelimiter = listEntryType.getDelimiter().replace("\\", "\\\\");
                append(output, "properties, \"", entry.getEntryName(), "\", \"", escapedDelimiter, "\", ", fieldName, ");\n");
            } else {
                append(output, fieldName, " = reader.get(properties, \"", entry.getEntryName(), "\", ", fieldName, ");\n");
            }
        }
    }

    private void generateAccessors(StringBuilder output) throws EntryException {
        GetterGenerator getterGenerator = new GetterGenerator(indent);

        for (PropertyEntry entry : entries) {
            String fieldName = nameConverter.toFieldName(entry.getEntryName());
            String methodNameRoot = fieldName.substring(0, 1).toUpperCase(Locale.ROOT) + fieldName.substring(1);

            List<String> lines = getterGenerator.generate(fieldName, methodNameRoot, entry.getEntryType());
            if (lines.size() > 0) {//TODO: remove this check
                output.append(indent).append("public ").append(lines.get(0)).append('\n');
            }
            for (int i = 1; i < lines.size(); i++) {
                output.append(indent).append(lines.get(i)).append('\n');
            }
            output.append('\n');
        }
    }

    private static void append(StringBuilder output, String... strings) {
        for (String s : strings) {
            output.append(s);
        }
    }
}
