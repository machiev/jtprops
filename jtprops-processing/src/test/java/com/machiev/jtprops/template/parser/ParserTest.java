package com.machiev.jtprops.template.parser;

import com.machiev.jtprops.template.entry.PropertyEntry;
import com.machiev.jtprops.template.exception.EntryException;
import com.machiev.jtprops.template.exception.IncorrectEntryFormatException;
import com.machiev.jtprops.template.exception.IncorrectTypeFormatException;
import com.machiev.jtprops.template.type.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collection;
import java.util.Iterator;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by machiev on 25.04.14.
 */
public class ParserTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldParseInteger() throws Exception {

        //given
        String[] testData= {
                "## type:Integer",
                "integerValue= 3",
                "",
        };
        Parser parser = new Parser(new LineIterator(testData), false, 100);

        //when
        Collection<PropertyEntry> entries = parser.parse();

        //then
        assertThat(entries.size(), is(1));
        PropertyEntry entry = entries.iterator().next();
        assertThat(entry.getEntryName(), is("integerValue"));
        assertThat(entry.getEntryType(), is(instanceOf(IntegerEntryType.class)));
        assertThat(entry.getEntryType().asString(), is("3"));
    }

    @Test
    public void shouldParseString() throws Exception {
        //given
        String[] testData= {
                "",
                "## type: String",
                "stringProperty=New application ",
        };
        Parser parser = new Parser(new LineIterator(testData), false, 100);

        //when
        Collection<PropertyEntry> entries = parser.parse();

        //then
        assertThat(entries.size(), is(1));
        PropertyEntry entry = entries.iterator().next();
        assertThat(entry.getEntryName(), is("stringProperty"));
        assertThat(entry.getEntryType(), is(instanceOf(StringEntryType.class)));
        assertThat(entry.getEntryType().asString(), is("\"New application\""));
    }

    @Test
    public void shouldParseBoolean() throws Exception {
        //given
        String[] testData= {
                "## type: boolean",
                "boolean_value = true",
        };
        Parser parser = new Parser(new LineIterator(testData), false, 100);

        //when
        Collection<PropertyEntry> entries = parser.parse();

        //then
        assertThat(entries.size(), is(1));
        PropertyEntry entry = entries.iterator().next();
        assertThat(entry.getEntryName(), is("boolean_value"));
        assertThat(entry.getEntryType(), is(instanceOf(BooleanEntryType.class)));
        assertThat(entry.getEntryType().asString(), is("true"));
    }

    @Test
    public void shouldParseListOfIntegers() throws Exception {
        //given
        String[] testData= {
                "",
                "##type:List<Integer>{,}",
                "integersList=1, 2,3",
        };
        Parser parser = new Parser(new LineIterator(testData), false, 100);

        //when
        Collection<PropertyEntry> entries = parser.parse();

        //then
        assertThat(entries.size(), is(1));
        PropertyEntry entry = entries.iterator().next();
        assertThat(entry.getEntryName(), is("integersList"));
        assertThat(entry.getEntryType(), is(instanceOf(ListEntryType.class)));
        assertThat(entry.getEntryType().asString(), is("1,2,3"));
    }

    @Test
    public void shouldParseEnum() throws Exception {
        //given
        String[] testData= {
                "## type: Enum<ENUM_TYPE>{ENUM_VAL1,ENUM_VAL2 , ENUM_VAL3}",
                "enumValue= ENUM_VAL2 ",
        };
        Parser parser = new Parser(new LineIterator(testData), false, 100);

        //when
        Collection<PropertyEntry> entries = parser.parse();

        //then
        assertThat(entries.size(), is(1));
        PropertyEntry entry = entries.iterator().next();
        assertThat(entry.getEntryName(), is("enumValue"));
        assertThat(entry.getEntryType(), is(instanceOf(EnumEntryType.class)));
        assertThat(entry.getEntryType().asString(), is("ENUM_VAL2"));
    }

    @Test
    public void shouldParseEnumAndThrowExceptionValueNotDefined() throws Exception {
        //given
        String[] testData= {
                "## type: Enum<ENUM_TYPE>{ENUM_VAL1,ENUM_VAL2 , ENUM_VAL3}",
                "enumValue= ENUM_VAL5 ",
        };
        Parser parser = new Parser(new LineIterator(testData), false, 100);

        //when
        expectedException.expect(EntryException.class);
        expectedException.expectMessage(containsString("ENUM_VAL5"));
        parser.parse();
    }

    @Test
    public void shouldParseEnumAndThrowExceptionNoValueDefined() throws Exception {
        //given
        String[] testData= {
                "## type: Enum<ENUM_TYPE>{}",
                "enumValue= ENUM_VAL5 ",
        };
        Parser parser = new Parser(new LineIterator(testData), false, 100);

        //when
        expectedException.expect(EntryException.class);
        parser.parse();

        //given
        testData = new String[] {
                "## type: Enum<ENUM_TYPE>{,}",
                "enumValue= ENUM_VAL5 ",
        };
        parser = new Parser(new LineIterator(testData), false, 100);

        //when
        expectedException.expect(IncorrectTypeFormatException.class);
        parser.parse();
    }

    private static class LineIterator implements Iterator<String> {

        private String[] testData;
        private int nextIndex;

        public LineIterator(String[] testData) {

            this.testData = testData;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < testData.length;
        }

        @Override
        public String next() {
            return testData[nextIndex++];
        }

        @Override
        public void remove() {
        }
    }
}
