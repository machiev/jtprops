package com.machiev.jtprops.template.io;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Test;

import java.io.StringReader;

/**
 * Created by machiev on 15.06.14.
 */
public class LineIteratorTest {

    @Test
    public void shouldNotIterateOverEmptyString() {
        //given
        LineIterator lineIterator = new LineIterator(new StringReader(""));

        //when
        boolean hasNext = lineIterator.hasNext();

        //then
        MatcherAssert.assertThat(hasNext, CoreMatchers.is(false));
    }

    @Test
    public void shouldIterateOverSingleLine() {
        //given
        LineIterator lineIterator = new LineIterator(new StringReader("not empty"));

        //when
        boolean hasNext = lineIterator.hasNext();

        //then
        MatcherAssert.assertThat(hasNext, CoreMatchers.is(true));
    }

    @Test
    public void shouldIterateOverSeveralLines() {
        //given
        LineIterator lineIterator = new LineIterator(new StringReader("not empty\nline2\nline3"));

        //when
        lineIterator.hasNext();
        lineIterator.next();
        lineIterator.hasNext();
        lineIterator.next();
        boolean hasNext = lineIterator.hasNext();
        String line = lineIterator.next();
        boolean finalNext = lineIterator.hasNext();

        //then
        MatcherAssert.assertThat(hasNext, CoreMatchers.is(true));
        MatcherAssert.assertThat(finalNext, CoreMatchers.is(false));
        MatcherAssert.assertThat(line, CoreMatchers.is("line3"));
    }
}
