package com.machiev.jtprops.template.generator;

import com.machiev.jtprops.template.entry.PropertyEntry;
import com.machiev.jtprops.template.exception.EntryException;
import com.machiev.jtprops.template.type.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by machiev on 03.05.14.
 */
public class PropertiesWrapperGeneratorTest {

    @Test
    public void test() {
        List<PropertyEntry> entries = new ArrayList<PropertyEntry>();
        entries.add(new PropertyEntry("entry1", new StringEntryType("entryValue")));
        entries.add(new PropertyEntry("entry2", new IntegerEntryType(111)));
        entries.add(new PropertyEntry("boolEntry", new BooleanEntryType(true)));
        List<IntegerEntryType> listElements = new ArrayList<IntegerEntryType>();
        listElements.add(new IntegerEntryType(100));
        listElements.add(new IntegerEntryType(200));
        entries.add(new PropertyEntry("listEntry", new ListEntryType<IntegerEntryType>(listElements, "Integer", ",", ",")));
        List<String> enumElements = new ArrayList<String>();
        enumElements.add("enum1");
        enumElements.add("enum2");
        entries.add(new PropertyEntry("enumEntry", new EnumEntryType("enumName", enumElements, "enum1")));

        PropertiesWrapperGenerator wrapperGenerator =
                new PropertiesWrapperGenerator(entries, "org.package", "ClassName", NameConvertingMode.DOTS_TO_CAMEL_CASE, 4);

        try {
            System.out.print(wrapperGenerator.generate());
        } catch (EntryException e) {
            e.printStackTrace();
        }
    }
}
