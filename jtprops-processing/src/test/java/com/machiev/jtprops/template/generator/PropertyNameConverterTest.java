package com.machiev.jtprops.template.generator;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by machiev on 05.04.14.
 */
public class PropertyNameConverterTest {

    @Test
    public void shouldConvertSinglePartPropertyName() throws Exception {
        //given
        PropertyNameConverter converter1 = new PropertyNameConverter(NameConvertingMode.DOTS_TO_UNDERSCORE);
        PropertyNameConverter converter2 = new PropertyNameConverter(NameConvertingMode.DOTS_TO_CAMEL_CASE);

        //when
        String convertedName1 = converter1.toFieldName("propertyName");
        String convertedName2 = converter2.toFieldName("propertyName");

        //then
        assertThat(convertedName1, is("propertyName"));
        assertThat(convertedName2, is("propertyName"));
    }

    @Test
    public void shouldConvertMultiPartPropertyName() throws Exception {
        //given
        PropertyNameConverter converter1 = new PropertyNameConverter(NameConvertingMode.DOTS_TO_UNDERSCORE);
        PropertyNameConverter converter2 = new PropertyNameConverter(NameConvertingMode.DOTS_TO_CAMEL_CASE);

        //when
        String convertedName1 = converter1.toFieldName("part1.part2");
        String convertedName2 = converter2.toFieldName("part1.part2");

        //then
        assertThat(convertedName1, is("part1_part2"));
        assertThat(convertedName2, is("part1Part2"));
    }

    @Test
    public void shouldConvertPropertyNameThatStartsFromNumber() throws Exception {
        //given
        PropertyNameConverter converter1 = new PropertyNameConverter(NameConvertingMode.DOTS_TO_UNDERSCORE);
        PropertyNameConverter converter2 = new PropertyNameConverter(NameConvertingMode.DOTS_TO_CAMEL_CASE);

        //when
        String convertedName1 = converter1.toFieldName("1.part1.part2");
        String convertedName2 = converter2.toFieldName("1.part1.part2");

        //then
        assertThat(convertedName1, is("$1_part1_part2"));
        assertThat(convertedName2, is("$1Part1Part2"));
    }

    @Test
    public void shouldConvertPropertyNameThatStartsFromDot() throws Exception {
        //given
        PropertyNameConverter converter1 = new PropertyNameConverter(NameConvertingMode.DOTS_TO_UNDERSCORE);
        PropertyNameConverter converter2 = new PropertyNameConverter(NameConvertingMode.DOTS_TO_CAMEL_CASE);

        //when
        String convertedName1 = converter1.toFieldName(".part1.part2");
        String convertedName2 = converter2.toFieldName(".part1.part2");

        //then
        assertThat(convertedName1, is("part1_part2"));
        assertThat(convertedName2, is("part1Part2"));
    }
}
