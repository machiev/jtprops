package com.machiev.jtprops.template.parser;

import com.machiev.jtprops.template.type.BooleanEntryType;
import com.machiev.jtprops.template.type.EntryType;
import com.machiev.jtprops.template.type.IntegerEntryType;
import com.machiev.jtprops.template.type.ListEntryType;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by machiev on 05.04.14.
 */
public class TypeFactoryTest {

    @Test
    public void shouldCreateInteger() throws Exception {
        EntryType integer = new TypeFactory("integer", " -123").create();

        assertThat(integer, is(instanceOf(IntegerEntryType.class)));
        assertThat(integer.asString(), is("-123"));
    }

    @Test
    public void shouldCreateIntegerFromEmptyString() throws Exception {
        EntryType integer = new TypeFactory("integer", "").create();

        assertThat(integer, is(instanceOf(IntegerEntryType.class)));
        assertThat(integer.asString(), is("0"));
    }

    @Test
    public void shouldCreateBoolean() throws Exception {
        EntryType bool = new TypeFactory("boolean", "false").create();

        assertThat(bool, is(instanceOf(BooleanEntryType.class)));
        assertThat(bool.asString(), is("false"));
    }

    @Test
    public void shouldCreateBooleanFromEmptyString() throws Exception {
        //given
        EntryType bool = new TypeFactory("boolean", "")
        //when
                .create();

        //then
        assertThat(bool, is(instanceOf(BooleanEntryType.class)));
        assertThat(bool.asString(), is("false"));
    }

    @Test
     public void shouldCreateListFromEmptyString() throws Exception {
        //given
        EntryType list = new TypeFactory("list<Integer>{,}", "")
        //when
                .create();

        //then
        assertThat(list, is(instanceOf(ListEntryType.class)));
        assertThat(list.asString(), is(""));
    }

    @Test
    public void shouldCreateListWithElements() throws Exception {
        //given
        EntryType list = new TypeFactory("list<Integer>{,}", "1, 2,3")
        //when
                .create();

        //then
        assertThat(list, is(instanceOf(ListEntryType.class)));
        assertThat(list.asString(), is("1,2,3"));
    }
}
