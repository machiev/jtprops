package com.machiev.jtprops.template.entry;


import org.junit.Assert;

/**
 * Created by machiev on 30.03.14.
 */
public class PropertyEntryTest {

    @org.junit.Test
    public void shouldBeEqualToItself() throws Exception {
        PropertyEntry entry1 = new PropertyEntry("propertyNumber1", null);
        PropertyEntry entry2 = entry1;

        Assert.assertEquals(entry1, entry2);
    }

    @org.junit.Test
    public void shouldBeEqualCaseInsensitive() throws Exception {
        PropertyEntry entry1 = new PropertyEntry("propertyNumber1", null);
        PropertyEntry entry2 = new PropertyEntry("propertynumber1", null);

        Assert.assertEquals(entry1, entry2);
    }

    @org.junit.Test
    public void shouldDiffer() throws Exception {
        PropertyEntry entry1 = new PropertyEntry("propertyNumber1", null);
        PropertyEntry entry2 = new PropertyEntry("propertynumber2", null);

        Assert.assertNotEquals(entry1, entry2);
    }

    @org.junit.Test
    public void shouldReturnTheSameHashCodeCaseInsensitive() throws Exception {
        PropertyEntry entry1 = new PropertyEntry("propertyNumber1", null);
        PropertyEntry entry2 = new PropertyEntry("propertynumber1", null);

        Assert.assertEquals(entry1.hashCode(), entry2.hashCode());
    }

}
