package com.machiev.jtprops.template.generator;

import com.machiev.jtprops.template.type.BooleanEntryType;
import com.machiev.jtprops.template.type.EnumEntryType;
import com.machiev.jtprops.template.type.IntegerEntryType;
import com.machiev.jtprops.template.type.StringEntryType;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by machiev on 03.05.14.
 */
public class FieldGeneratorTest {

    private FieldGenerator generator;

    @Before
    public void setUp() {
        generator = new FieldGenerator();
    }

    @Test
    public void shouldGenerateIntField() throws Exception {
        //given
        String name = "field1";
        IntegerEntryType integerEntryType = new IntegerEntryType(1);

        //when
        String generatedField = generator.generate(name, integerEntryType);

        //then
        assertThat(generatedField, is("int field1 = 1;"));
    }

    @Test
    public void shouldGenerateStringField() throws Exception {
        //given
        String name = "stringField";
        StringEntryType stringEntryType = new StringEntryType("stringVal");

        //when
        String generatedField = generator.generate(name, stringEntryType);

        //then
        assertThat(generatedField, is("String stringField = \"stringVal\";"));
    }

    @Test
    public void testGenerate2() throws Exception {
        //given
        String name = "boolField";
        BooleanEntryType booleanEntryType = new BooleanEntryType(false);

        //when
        String generatedField = generator.generate(name, booleanEntryType);

        //then
        assertThat(generatedField, is("boolean boolField = false;"));
    }

    @Test
    public void shouldGenerateEnumField() throws Exception {
        //given
        String name = "enumField";
        List<String> enumValues = new ArrayList<String>();
        enumValues.add("value1");
        enumValues.add("value2");
        EnumEntryType enumEntryType = new EnumEntryType("enumName", enumValues, "value2");

        //when
        String generatedField = generator.generate(name, enumEntryType);

        //then
        assertThat(generatedField, is("enumName enumField = enumName.value2;"));
    }

    @Test
    public void shouldGenerateEnumDefinition() throws Exception {
        //given
        List<String> enumValues = new ArrayList<String>();
        enumValues.add("value1");
        enumValues.add("value2");
        EnumEntryType enumEntryType = new EnumEntryType("enumName", enumValues, "value2");

        //when
        String generatedEnum = generator.generateEnumDefinition(enumEntryType);

        //then
        assertThat(generatedEnum, is("enum enumName { value1, value2 }"));
    }
}
