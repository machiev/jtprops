package com.machiev.jtprops.annotation.processor;

import com.machiev.jtprops.model.AccessorToNameConversionMode;
import org.junit.Test;

import static com.machiev.jtprops.model.AccessorToNameConversionMode.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by machiev on 03.09.14.
 */
public class NamesConverterTest {

    @Test
    public void shouldStripGetOrIsPrefix() {
        //given
        String methodName1 = "getName";
        String methodName2 = "get_Name";
        String methodName3 = "get_name";
        String methodName4 = "isValid";
        String methodName5 = "is__Valid";
        String methodName6 = "is__valid";

        //when
        String converted1 = NamesConverter.methodToFieldName(methodName1);
        String converted2 = NamesConverter.methodToFieldName(methodName2);
        String converted3 = NamesConverter.methodToFieldName(methodName3);
        String converted4 = NamesConverter.methodToFieldName(methodName4);
        String converted5 = NamesConverter.methodToFieldName(methodName5);
        String converted6 = NamesConverter.methodToFieldName(methodName6);

        //then
        assertThat(converted1, is("name"));
        assertThat(converted2, is("name"));
        assertThat(converted3, is("name"));
        assertThat(converted4, is("valid"));
        assertThat(converted5, is("valid"));
        assertThat(converted6, is("valid"));
    }

    @Test
    public void shouldRemoveUnderscoreWhenThereWasPrefix() {
        //given
        String methodName1 = "__$__";
        String methodName2 = "_1";

        //when
        String converted1 = NamesConverter.methodToFieldName(methodName1);
        String converted2 = NamesConverter.methodToFieldName(methodName2);

        //then
        assertThat(converted1, is("__$__"));
        assertThat(converted2, is("_1"));
    }

    @Test
    public void shouldRemoveRemoveKnownPrefixOnly() {
        //given
        String methodName1 = "getter";
        String methodName2 = "island";
        String methodName3 = "isLand";

        //when
        String converted1 = NamesConverter.methodToFieldName(methodName1);
        String converted2 = NamesConverter.methodToFieldName(methodName2);
        String converted3 = NamesConverter.methodToFieldName(methodName3);

        //then
        assertThat(converted1, is("getter"));
        assertThat(converted2, is("island"));
        assertThat(converted3, is("land"));
    }

    @Test
    public void shouldConvertMethodNameToUnderscoredKeyName() {
        //given
        String methodName1 = "name";
        String methodName2 = "Name";
        String methodName3 = "nameName";
        String methodName4 = "getName";
        String methodName5 = "getNameName";
        String methodName6 = "getName_Name";
        String methodName7 = "get_Name_name";
        String methodName8 = "get_Name__name";
        AccessorToNameConversionMode convertingMode = CAMELCASE_TO_UNDERSCORE;

        //when
        String converted1 = NamesConverter.methodToKeyName(methodName1, convertingMode, false);
        String converted2 = NamesConverter.methodToKeyName(methodName2, convertingMode, false);
        String converted3 = NamesConverter.methodToKeyName(methodName3, convertingMode, false);
        String converted4 = NamesConverter.methodToKeyName(methodName4, convertingMode, false);
        String converted5 = NamesConverter.methodToKeyName(methodName5, convertingMode, false);
        String converted6 = NamesConverter.methodToKeyName(methodName6, convertingMode, false);
        String converted7 = NamesConverter.methodToKeyName(methodName7, convertingMode, false);
        String converted8 = NamesConverter.methodToKeyName(methodName8, convertingMode, false);

        //then
        assertThat(converted1, is("name"));
        assertThat(converted2, is("name"));
        assertThat(converted3, is("name_name"));
        assertThat(converted4, is("name"));
        assertThat(converted5, is("name_name"));
        assertThat(converted6, is("name__name"));
        assertThat(converted7, is("name_name"));
        assertThat(converted8, is("name__name"));
    }

    @Test
    public void shouldConvertMethodNameToSeparatorKeyName() {
        //given
        String methodName1 = "getNameNumberOne";

        //when
        String converted1 = NamesConverter.methodToKeyName(methodName1, CAMELCASE_TO_UNDERSCORE, true);
        String converted2 = NamesConverter.methodToKeyName(methodName1, CAMELCASE_TO_DOTS, true);
        String converted3 = NamesConverter.methodToKeyName(methodName1, CAMELCASE_TO_HYPHEN, true);

        //then
        assertThat(converted1, is("name_number_one"));
        assertThat(converted2, is("name.number.one"));
        assertThat(converted3, is("name-number-one"));
    }

    @Test
    public void shouldConvertMethodNameToSeparatorKeyNameAndRemoveDuplicatedSeparators() {
        //given
        String methodName1 = "get_Name_Number__One";

        //when
        String converted1 = NamesConverter.methodToKeyName(methodName1, CAMELCASE_TO_UNDERSCORE, true);

        //then
        assertThat(converted1, is("name_number_one"));
    }

    @Test
    public void shouldConvertUnderscoredMethodNameToSeparatorKeyName() {
        //given
        String methodName1 = "get_name_number_one";

        //when
        String converted1 = NamesConverter.methodToKeyName(methodName1, UNDERSCORE_TO_DOTS, true);
        String converted2 = NamesConverter.methodToKeyName(methodName1, UNDERSCORE_TO_HYPHEN, true);
        String converted3 = NamesConverter.methodToKeyName(methodName1, CAMELCASE_TO_HYPHEN, true);

        //then
        assertThat(converted1, is("name.number.one"));
        assertThat(converted2, is("name-number-one"));
        assertThat(converted3, is("name_number_one"));
    }

    @Test
    public void shouldConvertUnderscoredMethodNameToSeparatorKeyNameNoDuplicates() {
        //given
        String methodName1 = "get_name__number__one";

        //when
        String converted1 = NamesConverter.methodToKeyName(methodName1, UNDERSCORE_TO_DOTS, true);
        String converted2 = NamesConverter.methodToKeyName(methodName1, UNDERSCORE_TO_HYPHEN, true);
        String converted3 = NamesConverter.methodToKeyName(methodName1, CAMELCASE_TO_HYPHEN, true);

        //then
        assertThat(converted1, is("name.number.one"));
        assertThat(converted2, is("name-number-one"));
        assertThat(converted3, is("name__number__one"));
    }

    @Test
    public void shouldNotConvertMethodName() {
        //given
        String methodName1 = "get_name_number_one";
        String methodName2 = "getName";

        //when
        String converted1 = NamesConverter.methodToKeyName(methodName1, NONE, true);
        String converted2 = NamesConverter.methodToKeyName(methodName2, NONE, true);

        //then
        assertThat(converted1, is("name_number_one"));
        assertThat(converted2, is("name"));
    }
}
