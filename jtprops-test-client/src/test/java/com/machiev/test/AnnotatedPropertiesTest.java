package com.machiev.test;

import com.machiev.jtprops.reader.SimplePropertyReader;
import com.machiev.jtprops.test.PropWrapper;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by machiev on 13.11.14.
 */
public class AnnotatedPropertiesTest {

    private Properties properties;
    private SimplePropertyReader reader;

    @Before
    public void setUp() {
        properties = new Properties();
        reader = new SimplePropertyReader();
    }

    @Test
    public void shouldReturnDefaults() {
        //given

        //when
        PropWrapper testProperties = new PropWrapper(properties, reader);

        //then
        assertThat(testProperties.getEnumProperty(), is(AnnotatedProperties.SomeEnum.ENUM1));
        assertThat(testProperties.getStringProperty(), is(""));
        assertThat(testProperties.getIntegerProperty(), is(42));
        assertThat(testProperties.getLongProperty(), is(1L));
        assertThat(testProperties.getBooleanProperty(), is(false));
        assertThat(testProperties.getIntegerList().size(), is(3));
        assertThat(testProperties.getIntegerList(), hasItems(1, 2, 4));
        assertTrue(testProperties.getLongList().isEmpty());
        assertThat(testProperties.getStringList().size(), is(3));
        assertThat(testProperties.getStringList(), hasItems("string1", "string2", "string3"));
    }

    @Test
    public void shouldReturnSetValues() {
        //given
        properties.setProperty("someEnumProperty", "ENUM2");
        properties.setProperty("string.property", "StringValue");
        properties.setProperty("integer.property", "11");
        properties.setProperty("long.property", "22");
        properties.setProperty("boolean.property", "true");
        properties.setProperty("integer.list", "10, 11, 12");
        properties.setProperty("long.list", "100, 200");
        properties.setProperty("string.list", "s1, s2");

        //when
        PropWrapper testProperties = new PropWrapper(properties, new SimplePropertyReader());

        //then
        assertThat(testProperties.getEnumProperty(), is(AnnotatedProperties.SomeEnum.ENUM2));
        assertThat(testProperties.getStringProperty(), is("StringValue"));
        assertThat(testProperties.getIntegerProperty(), is(11));
        assertThat(testProperties.getLongProperty(), is(22L));
        assertThat(testProperties.getBooleanProperty(), is(true));
        assertThat(testProperties.getIntegerList().size(), is(3));
        assertThat(testProperties.getIntegerList(), hasItems(10, 11, 12));
        assertThat(testProperties.getLongList().size(), is(2));
        assertThat(testProperties.getLongList(), hasItems(100L, 200L));
        assertThat(testProperties.getStringList().size(), is(2));
        assertThat(testProperties.getStringList(), hasItems("s1", "s2"));
    }

}
