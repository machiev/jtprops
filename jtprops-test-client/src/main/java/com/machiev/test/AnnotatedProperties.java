package com.machiev.test;

import com.machiev.jtprops.model.ListProperty;
import com.machiev.jtprops.model.PropertiesClass;
import com.machiev.jtprops.model.Property;

import java.util.List;

import static com.machiev.jtprops.model.OptionalFlag.*;

/**
 * Created by machiev on 19.07.14.
 */
@PropertiesClass(packageName = "com.machiev.jtprops.test", className = "PropWrapper")
public interface AnnotatedProperties {

    public enum SomeEnum {
        ENUM1,
        ENUM2;

        private int aField;
    }

    @Property(name = "someEnumProperty")
    SomeEnum getEnumProperty();

    @Property
    String getStringProperty();

    @Property(defaultValue = "42")
    Integer getIntegerProperty();

    @Property(defaultValue = "1L")
    Long getLongProperty();

    @Property
    Boolean getBooleanProperty();

    @ListProperty(name = "integer.list", defaultValue = {"1", "2", "4"}, generateChecker = FALSE)
    List<Integer> getIntegerList();

    @ListProperty(name = "long.list")
    List<Long> getLongList();

    @ListProperty(defaultValue = {"42.24", "1.12345"})
    List<Double> getDoubleList();

    @ListProperty(defaultValue = {"string1", "string2", "string3"})
    List<String> getStringList();
}

@PropertiesClass(packageName = "com.machiev.test", className = "PropWrapper1", abstractClass = TRUE, extendedInterfaceName = "ExtendedWrapper")
interface AnnotatedProperties1 {

    public enum SomeEnum {
        ENUM1,
        ENUM2;
    }

    @Property(name = "someEnumProperty")
    SomeEnum getEnumProperty();

    @Property
    String getStringProperty();

    @Property
    int getIntegerProperty();

    @Property
    boolean getBooleanProperty();

    @ListProperty(name = "integer.list", defaultValue = {"1", "2", "4"})
    List<Integer> getIntegerList();

    @ListProperty(defaultValue = {"string1", "string2", "string3"})
    List<String> getStringList();
}

@PropertiesClass(packageName = "com.machiev.test", className = "PropWrapper4", generateCheckers = FALSE)
abstract class AnnotatedProperties4 {

    public enum SomeEnum {
        ENUM1,
        ENUM2;
    }

    @Property(name = "someEnumProperty")
    public abstract SomeEnum getEnumProperty();

    @Property
    public abstract String getStringProperty();

    @Property
    public abstract int getIntegerProperty();

    @Property
    public abstract boolean getBooleanProperty();

    @ListProperty(name = "integer.list", defaultValue = {"1", "2", "4"})
    public abstract List<Integer> getIntegerList();

    @ListProperty(defaultValue = {"string1", "string2", "string3"})
    public abstract List<String> getStringList();
}
