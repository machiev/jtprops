package com.machiev.test.subtest;

/**
 * Created by machiev on 19.07.14.
 */
public enum SomeEnum {
    ENUM1("text1"),
    ENUM2("text2");

    private String text;

    private SomeEnum(String text) {
        this.text = text;
    }
}
