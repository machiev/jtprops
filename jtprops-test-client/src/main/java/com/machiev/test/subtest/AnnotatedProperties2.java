package com.machiev.test.subtest;

import com.machiev.jtprops.model.AccessorToNameConversionMode;
import com.machiev.jtprops.model.ListProperty;
import com.machiev.jtprops.model.PropertiesClass;
import com.machiev.jtprops.model.Property;

import java.util.List;

@PropertiesClass(packageName = "com.machiev.jtprops.test", className = "PropWrapper2", conversionMode = AccessorToNameConversionMode.CAMELCASE_TO_UNDERSCORE)
public interface AnnotatedProperties2 {

    @Property(name = "someEnumProperty", defaultValue = "ENUM2")
    SomeEnum getEnumProperty();

    @Property(defaultValue = "empty")
    String getStringProperty();

    @Property(defaultValue = "42")
    Integer getIntegerProperty();

    @Property(defaultValue = "true")
    Boolean getBooleanProperty();

    @ListProperty(name = "integer.list", defaultValue = {"1", "2", "4"})
    List<Integer> getIntegerList();

    @ListProperty(defaultValue = {"string1", "string2", "string3"})
    List<String> getStringList();

    @PropertiesClass(packageName = "com.machiev.jtprops.subtest", className = "PropWrapper3")
    public interface AnnotatedProperties3 {

        @Property(name = "someEnumProperty", defaultValue = "ENUM2")
        SomeEnum getEnumProperty();

        @Property(defaultValue = "empty")
        String getStringProperty();

        @Property(defaultValue = "42")
        Integer getIntegerProperty();

        @Property(defaultValue = "true")
        Boolean getBooleanProperty();

        @ListProperty(name = "integer.list", defaultValue = {"1", "2", "4"})
        List<Integer> getIntegerList();

        @ListProperty(defaultValue = {"string1", "string2", "string3"})
        List<String> getStringList();
    }
}

