package com.machiev;

import com.machiev.jtprops.reader.SimplePropertyReader;
import com.machiev.jtprops.test.PropWrapper;

import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty("someEnumProperty", "ENUM2");
        properties.setProperty("long.property", "22");

        PropWrapper testProperties = new PropWrapper(properties, new SimplePropertyReader());

        System.out.println("getEnumProperty(): " + testProperties.getEnumProperty());
        System.out.println("getStringProperty(): " + testProperties.getStringProperty());
        System.out.println("getIntegerProperty(): " + testProperties.getIntegerProperty());
        System.out.println("getLongProperty(): " + testProperties.getLongProperty());
        System.out.println("getBooleanProperty(): " + testProperties.getBooleanProperty());
        System.out.println("getIntegerList(): " + testProperties.getIntegerList());
        System.out.println("getLongList(): " + testProperties.getLongList());
        System.out.println("getStringList(): " + testProperties.getStringList());
    }
}
