/**
 * Defines filter interface for filtering properties before conversion performed by {@link com.machiev.jtprops.reader.PropertyReader}
 */
package com.machiev.jtprops.filter;
