/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.filter;

import java.util.Properties;

/**
 * Filters property based on some external rules.
 * Intended for selection of properties relevant to some application context.
 */
public interface PropertyFilter {

    /**
     * Returns a property value if properties contains supplied key and a property is not to be filtered out, otherwise returns <code>null</code>.
     * @param properties the properties to obtain value from.
     * @param key the property key.
     * @return the value of the property with the specified key value.
     */
    String getProperty(Properties properties, String key);

    /**
     * Returns a property value if properties contains supplied key and a property is not to be filtered out, otherwise returns <code>defaultValue</code>.
     * @param properties the properties to obtain value from.
     * @param key the property key.
     * @param defaultValue the default value used when the key is not found.
     * @return the value of the property with the specified key value or default value.
     */
    String getProperty(Properties properties, String key, String defaultValue);
}
