/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.filter;

import java.util.Properties;

/**
 * Implementation of default pass through filter which does no filtering.
 */
public class PassThroughFilter implements PropertyFilter {

    /**
     * Returns result of the {@link java.util.Properties#getProperty(String)} method call.
     * @param properties the properties to obtain value from.
     * @param key the property key.
     * @return the value of the property with the specified key value.
     *
     * @see com.machiev.jtprops.filter.PropertyFilter#getProperty(java.util.Properties, String)
     * @see java.util.Properties#getProperty(String)
     */
    @Override
    public String getProperty(Properties properties, String key) {
        return properties.getProperty(key);
    }

    /**
     * Returns result of the {@link java.util.Properties#getProperty(String, String)} method call.
     * @param properties the properties to obtain value from.
     * @param key the property key.
     * @return the value of the property with the specified key value.
     *
     * @see com.machiev.jtprops.filter.PropertyFilter#getProperty(java.util.Properties, String)
     * @see java.util.Properties#getProperty(String)
     */
    @Override
    public String getProperty(Properties properties, String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }
}
