/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.reader;

import com.machiev.jtprops.filter.PassThroughFilter;
import com.machiev.jtprops.filter.PropertyFilter;

import java.util.*;

/**
 * Created by machiev on 14.05.14.
 */

/**
 * Simple property reader with optional filtering.
 */
public class SimplePropertyReader implements PropertyReader {

    private PropertyFilter filter;

    /**
     * Constructs <code>SimplePropertyReader</code> with {@link com.machiev.jtprops.filter.PassThroughFilter}.
     */
    public SimplePropertyReader() {
        filter = new PassThroughFilter();
    }

    public SimplePropertyReader(PropertyFilter filter) {
        this.filter = filter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPropertyFilter(PropertyFilter filter) {
        this.filter = filter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String get(Properties properties, String key, String defaultValue) {
        String value = filter.getProperty(properties, key);
        if (value == null) {
            value = defaultValue;
        }
        return value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public char get(Properties properties, String key, char defaultValue) {
        String charString = filter.getProperty(properties, key, "");

        if (!charString.isEmpty()) {
            return charString.trim().charAt(0);
        }
        return defaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public short get(Properties properties, String key, short defaultValue) {
        String shortString = filter.getProperty(properties, key, "");

        if (!shortString.isEmpty()) {
            return Short.valueOf(shortString.trim());
        }
        return defaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int get(Properties properties, String key, int defaultValue) {
        String intString = filter.getProperty(properties, key, "");

        if (!intString.isEmpty()) {
            return Integer.valueOf(intString.trim());
        }
        return defaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long get(Properties properties, String key, long defaultValue) {
        String longString = filter.getProperty(properties, key, "");

        if (!longString.isEmpty()) {
            return Long.valueOf(longString.trim());
        }
        return defaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double get(Properties properties, String key, double defaultValue) {
        String doubleString = filter.getProperty(properties, key, "");

        if (!doubleString.isEmpty()) {
            return Double.valueOf(doubleString.trim());
        }
        return defaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean get(Properties properties, String key, boolean defaultValue) {
        String boolString = filter.getProperty(properties, key);

        if (boolString != null) {
            return Boolean.valueOf(boolString.trim());
        }
        return defaultValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getList(Properties properties, String key, String separator, String... defaultValues) {
        String listToParse = filter.getProperty(properties, key);
        List<String> stringList = new ArrayList<String>();

        if (listToParse != null) {
            String[] stringArray = listToParse.split(separator);

            for (String element : stringArray) {
                stringList.add(element.trim());
            }
        } else {
            if (defaultValues != null && defaultValues.length > 0) {
                Collections.addAll(stringList, defaultValues);
            }
        }

        return stringList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Integer> getList(Properties properties, String key, String separator, Integer... defaultValues) {
        String listToParse = filter.getProperty(properties, key);
        List<Integer> intList = new ArrayList<Integer>();

        if (listToParse != null) {
            String[] stringArray = listToParse.split(separator);
            for (String element : stringArray) {
                intList.add(Integer.valueOf(element.trim()));
            }
        } else {
            if (defaultValues != null && defaultValues.length > 0) {
                Collections.addAll(intList, defaultValues);
            }
        }

        return intList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Long> getList(Properties properties, String key, String separator, Long... defaultValues) {
        String listToParse = filter.getProperty(properties, key);
        List<Long> longList = new ArrayList<Long>();

        if (listToParse != null) {
            String[] stringArray = listToParse.split(separator);
            for (String element : stringArray) {
                longList.add(Long.valueOf(element.trim()));
            }
        } else {
            if (defaultValues != null && defaultValues.length > 0) {
                Collections.addAll(longList, defaultValues);
            }
        }

        return longList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Double> getList(Properties properties, String key, String separator, Double... defaultValues) {
        String listToParse = filter.getProperty(properties, key);
        List<Double> doubleList = new ArrayList<Double>();

        if (listToParse != null) {
            String[] stringArray = listToParse.split(separator);
            for (String element : stringArray) {
                doubleList.add(Double.valueOf(element.trim()));
            }
        } else {
            if (defaultValues != null && defaultValues.length > 0) {
                Collections.addAll(doubleList, defaultValues);
            }
        }

        return doubleList;
    }

    /**
     * Returns property value converted to <code>Enum</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * <p>Property value is treated in case insensitive manner during conversion.</p>
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    @Override
    public <E extends Enum<E>> E get(Properties properties, String key, E defaultValue) {
        String enumString = filter.getProperty(properties, key);

        if (enumString != null) {
            for (E enumConst : defaultValue.getDeclaringClass().getEnumConstants()) {
                if (enumConst.name().equalsIgnoreCase(enumString)) {
                    return enumConst;
                }
            }
        }

        return defaultValue;
    }
}
