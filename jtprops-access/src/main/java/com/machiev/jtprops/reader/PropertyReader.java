/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.reader;

import com.machiev.jtprops.filter.PropertyFilter;

import java.util.List;
import java.util.Properties;

/**
 * Interface for accessing and converting properties from {@link java.util.Properties} class.
 * Additionally properties can be filtered by {@link com.machiev.jtprops.filter.PropertyFilter}.
 */
public interface PropertyReader {

    /**
     * Sets property filter.
     * @param filter filter object to be used when accessing properties.
     */
    void setPropertyFilter(PropertyFilter filter);

    /**
     * Returns property value as <code>String</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    String get(Properties properties, String key, String defaultValue);

    /**
     * Returns property value converted to <code>char</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    char get(Properties properties, String key, char defaultValue);

    /**
     * Returns property value converted to <code>short</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    short get(Properties properties, String key, short defaultValue);

    /**
     * Returns property value converted to <code>int</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    int get(Properties properties, String key, int defaultValue);

    /**
     * Returns property value converted to <code>long</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    long get(Properties properties, String key, long defaultValue);

    /**
     * Returns property value converted to <code>double</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    double get(Properties properties, String key, double defaultValue);

    /**
     * Returns property value converted to <code>boolean</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    boolean get(Properties properties, String key, boolean defaultValue);

    /**
     * Returns property value converted to <code>List&lt;String&gt;</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * Conversion is performed by splitting a property string using provided separator.
     * @param properties property list.
     * @param key property key.
     * @param separator separator string.
     * @param defaultValues an array of default values.
     * @return the value of the found property or default value.
     */
    List<String> getList(Properties properties, String key, String separator, String... defaultValues);

    /**
     * Returns property value converted to <code>List&lt;Integer&gt;</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * Conversion is performed by splitting a property string using provided separator.
     * @param properties property list.
     * @param key property key.
     * @param separator separator string.
     * @param defaultValues an array of default values.
     * @return the value of the found property or default value.
     */
    List<Integer> getList(Properties properties, String key, String separator, Integer... defaultValues);

    /**
     * Returns property value converted to <code>List&lt;Long&gt;</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * Conversion is performed by splitting a property string using provided separator.
     * @param properties property list.
     * @param key property key.
     * @param separator separator string.
     * @param defaultValues an array of default values.
     * @return the value of the found property or default value.
     */
    List<Long> getList(Properties properties, String key, String separator, Long... defaultValues);

    /**
     * Returns property value converted to <code>List&lt;Double&gt;</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * Conversion is performed by splitting a property string using provided separator.
     * @param properties property list.
     * @param key property key.
     * @param separator separator string.
     * @param defaultValues an array of default values.
     * @return the value of the found property or default value.
     */
    List<Double> getList(Properties properties, String key, String separator, Double... defaultValues);

    /**
     * Returns property value converted to <code>Enum</code> object from property list found by a key.
     * If the key is not found default value is returned.
     * @param properties property list.
     * @param key property key.
     * @param defaultValue a default value.
     * @return the value of the found property or default value.
     */
    <E extends Enum<E>> E get(Properties properties, String key, E defaultValue);
}
