/**
 * Defines convenient interface for converter from String property value to a desired type.
 */
package com.machiev.jtprops.reader;
