/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark an interface or a class that will be a parent of generated wrapper class for properties access.
 * <p>
 * Annotation is processed by JTProps annotation processor.
 * </p>
 * This annotation will cause generation of class implementing methods accessing properties.
 * The annotated interface or class will become a parent of a generated class.
 * Methods needs to be annotated by {@link com.machiev.jtprops.model.Property} or {@link com.machiev.jtprops.model.ListProperty}.
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface PropertiesClass {

    /**
     * Name of generated class' package.
     */
    String packageName();

    /**
     * Name of generated class.
     */
    String className();

    /**
     * Flag indicating that generated class will be abstract or not. By default concrete class is generated.
     */
    OptionalFlag abstractClass() default OptionalFlag.FALSE;

    /**
     * Conversion mode for key name generation from annotated methods. This overrides annotation processor option for an annotated class.
     */
    AccessorToNameConversionMode conversionMode() default AccessorToNameConversionMode.UNSET;

    /**
     * Flag indicating that checker methods are to be generated.
     * Checker method allows to determine if a property was provided or not in a .properties file.
     * This setting can be overridden on method basis by a flag present in method annotation e.g. {@link com.machiev.jtprops.model.Property}
     *
     * @see com.machiev.jtprops.model.Property
     */
    OptionalFlag generateCheckers() default OptionalFlag.TRUE;

    /**
     * When this property is specified (not empty) an interface is generated that extends an interface annotated by this annotation.
     * This extended interface will contain additional methods e.g. checker methods.
     * This property has no meaning when an annotated type is a class or generation of checker methods is disabled.
     */
    String extendedInterfaceName() default "";
}
