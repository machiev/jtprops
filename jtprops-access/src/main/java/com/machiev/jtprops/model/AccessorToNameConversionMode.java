/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.model;

/**
 * <p>
 * Conversion mode is used during conversion of accessor method name to property key name.
 * E.g. <code>getSomeProperty()</code> may be converted to "some.property", "some_property"
 * or "some-property" depending on selected mode.
 * </p>
 * Camel case conversion modes are intended for camel case method naming style and underscore
 * is dedicated to underscore method naming style, e.g. <code>getSomeProperty()</code> vs. <code>get_some_property()</code>.
 * In any case method's known prefixes such as 'get', 'is' are removed regardless the convention.
 */
public enum AccessorToNameConversionMode {

    UNSET(""),
    /**
     * No conversion will be performed method name will be used for property name after striping known prefixes.
     * <p>
     * E.g. <code>getSomeProperty()</code> will be converted to "someProperty",
     * <code>get_some_property()</code> will be converted to "some_property".
     * </p>
     */
    NONE(""),
    /**
     * Conversion of a camel case method name to a dot '.' separated key name.
     * <p>
     * E.g. <code>getSomeProperty()</code> will be converted to "some.property".
     * </p>
     */
    CAMELCASE_TO_DOTS("."),
    /**
     * Conversion of a camel case method name to a underscore '_' separated key name.
     * <p>
     * E.g. <code>getSomeProperty()</code> will be converted to "some_property".
     * </p>
     */
    CAMELCASE_TO_UNDERSCORE("_"),
    /**
     * Conversion of a camel case method name to a hyphen '-' separated key name.
     * <p>
     * E.g. <code>getSomeProperty()</code> will be converted to "some-property".
     * </p>
     */
    CAMELCASE_TO_HYPHEN("-"),
    /**
     * Conversion of an underscore method name to a dot '.' separated key name.
     * <p>
     * E.g. <code>get_some_property()</code> will be converted to "some.property".
     * </p>
     */
    UNDERSCORE_TO_DOTS("."),
    /**
     * Conversion of an underscore method name to a hyphen '-' separated key name.
     * <p>
     * E.g. <code>get_some_property()</code> will be converted to "some-property".
     * </p>
     */
    UNDERSCORE_TO_HYPHEN("-");

    private final String separator;

    private AccessorToNameConversionMode(String separator) {
        this.separator = separator;
    }

    /**
     * Returns separator related to a given conversion mode.
     * @return separator string.
     */
    public String getSeparator() {
        return separator;
    }
}
