/**
 * Defines annotations processed by Java Typed Properties (JTProps) annotation processor.
 *
 * Annotations mark wrapper interface or class accessing properties.
 * There is class level annotation {@link com.machiev.jtprops.model.PropertiesClass} and
 * method level annotations {@link com.machiev.jtprops.model.Property}, {@link com.machiev.jtprops.model.ListProperty}.
 */
package com.machiev.jtprops.model;
