/*
 * Copyright 2014 Maciej Nowakowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.machiev.jtprops.model;

/**
 * Boolean flag with additional third state <code>UNSET</code>.
 * Used in annotations for optional properties.
 *
 * @see PropertiesClass#abstractClass()
 * @see Property#generateChecker()
 */
public enum OptionalFlag {
    TRUE,
    FALSE,
    UNSET
}
