package com.machiev.jtprops.reader;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Properties;

import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.*;

/**
 * Created by machiev on 09.06.14.
 */
public class SimplePropertyReaderTest {

    enum ENUM1 {ENUM_VAL1, ENUM_VAL2}
    private PropertyReader reader;

    @Before
    public void setUp() {
        reader = new SimplePropertyReader();
    }

    @Test
    public void shouldReturnEnumValue() throws Exception {
        //given
        Properties properties = new Properties();
        properties.setProperty("enum_key", "ENUM_VAL1");

        //when
        ENUM1 readEnum = reader.get(properties, "enum_key", ENUM1.ENUM_VAL2);

        //then
        assertThat(readEnum, is(ENUM1.ENUM_VAL1));
    }

    @Test
    public void shouldReturnStringList() throws Exception {
        //given
        Properties properties = new Properties();
        properties.setProperty("string.list", "string1, string2,string3");

        //when
        List<String> stringList = reader.getList(properties, "string.list", ",", (String[])null);

        //then
        assertThat(stringList.size(), is(3));
        assertThat(stringList, hasItems("string1", "string2", "string3"));
    }

    @Test
    public void shouldReturnStringListDefaults() throws Exception {
        //given
        Properties properties = new Properties();

        //when
        List<String> stringList = reader.getList(properties, "string.list", ",", "string1", "string2");

        //then
        assertThat(stringList.size(), is(2));
        assertThat(stringList, hasItems("string1", "string2"));
    }

    @Test
    public void shouldReturnEmptyStringList() throws Exception {
        //given
        Properties properties = new Properties();

        //when
        List<String> stringList = reader.getList(properties, "string.list", ",", (String[])null);

        //then
        assertTrue(stringList.isEmpty());
    }
}
